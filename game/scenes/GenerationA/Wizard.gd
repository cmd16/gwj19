extends KinematicBody2D

export (int) var speed = 1200
export (int) var jump_speed = -1800
export (int) var gravity = 4000
export (float) var jump_release = 0.1

signal start_fire(src)
signal start_cut(dir, loc)
signal start_bomb(loc)
signal start_beam(dir, loc)
signal start_beam_bomb(dir, loc)

const UP_LEFT = Vector2(-1, -1)
const UP = Vector2(0, -1)
const UP_RIGHT = Vector2(1, -1)
const RIGHT = Vector2(1, 0)
const DOWN_RIGHT = Vector2(1, 1)
const DOWN = Vector2(0, 1)
const DOWN_LEFT = Vector2(-1, 1)
const LEFT = Vector2(-1, 0)
const circle_dirs = [UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT]
const diagonals = [UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT]
const cardinals = [UP, RIGHT, DOWN, LEFT]

var velocity = Vector2.ZERO

func cut(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_cut", dir, loc)


func bomb(loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_bomb", loc)

func beam(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_beam", dir, loc)

func bomb_beam(loc=null):
	# NOT USED, TOO CHAOTIC
	if loc == null:
		loc = get_parent().world_to_map(position)
	bomb(loc)
	for dir in cardinals:
		beam(dir, loc + dir)

func beam_bomb(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_beam_bomb", dir, loc)

func fire(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	var dest = loc + dir
	emit_signal("start_fire", dest)

func get_input():
	velocity.x = 0
	
	# actions that don't use direction
#	if Input.is_action_just_pressed("bomb"):
#		if not Input.is_action_pressed("beam"):
#			bomb()
	if true:
		var left = Input.is_action_pressed("left")
		var right = Input.is_action_pressed("right")
		var up = Input.is_action_pressed("up")
		var down = Input.is_action_pressed("down")
		var dir
		
		if left:
			$Sprite.scale = Vector2(1, 1)
		elif right:
			$Sprite.scale = Vector2(-1, 1)
		
		if up:
			if left:
				dir = UP_LEFT
			elif right:
				dir = UP_RIGHT
			else:
				dir = UP
		elif down:
			if left:
				dir = DOWN_LEFT
			elif right:
				dir = DOWN_RIGHT
			else:
				dir = DOWN
		elif left:
			dir = LEFT
		elif right:
			dir = RIGHT
		
#		print(dir)
		
		if Input.is_action_pressed("attack"):
#			print("ATTACK!!!")
			if get_parent().bomb() and not get_parent().lazer() and Input.is_action_just_pressed("attack"):
				bomb(dir)
#				print("boom")
			elif get_parent().lazer() and dir != null:
				if get_parent().bomb():
					beam_bomb(dir)
					print("boom but there")
				else:
					beam(dir)
					print("beaming")
			elif get_parent().fire() and dir != null:
				fire(dir)
				print("and it burns, burns burns")
			if get_parent().cut() and dir != null:
				cut(dir)
				print("chop chop")
#			print(get_parent().bomb(), get_parent().lazer(), get_parent().fire(), get_parent().cut())
#			print(dir)
	if Input.is_action_pressed("right"):
		velocity.x += speed
	if Input.is_action_pressed("left"):
		velocity.x -= speed


func _physics_process(delta):
	get_input()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			velocity.y = -jump_speed
	if Input.is_action_just_released("jump"):
		if velocity.y < 0:
			velocity.y *= jump_release

func _input(event):
	pass
#	if event.is_action_pressed("fire"):
#		$AnimationPlayer.play("Fire")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
