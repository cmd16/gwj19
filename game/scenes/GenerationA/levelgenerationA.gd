extends TileMap

export (PackedScene) var AnimatedBombSprite
export var own_path = "res://game/levels/fixed0.tscn"

enum MODIFIER_KEY { MODKEY_SHIFT , MODKEY_ALT, MODKEY_CTRL }

const full_grass = Vector2(0, 0)
const grass_sw = Vector2(3, 0)
const grass_se = Vector2(4, 0)
const grass_ne = Vector2(5, 0)
const grass_nw = Vector2(6, 0)
const full_soil = Vector2(0, 1)
const soil_sw = Vector2(3, 1)
const soil_se = Vector2(4, 1)
const soil_ne = Vector2(5, 1)
const soil_nw = Vector2(6, 1)
const cell_types = [full_grass, grass_sw, grass_se, grass_ne, grass_nw, 
					full_soil, soil_sw, soil_se, soil_ne, soil_nw]
const SENTINEL_VEC = Vector2.INF
const CLEAR_VEC = Vector2(-1, -1)

const UP_LEFT = Vector2(-1, -1)
const UP = Vector2(0, -1)
const UP_RIGHT = Vector2(1, -1)
const RIGHT = Vector2(1, 0)
const DOWN_RIGHT = Vector2(1, 1)
const DOWN = Vector2(0, 1)
const DOWN_LEFT = Vector2(-1, 1)
const LEFT = Vector2(-1, 0)
const circle_dirs = [UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT]
const diagonals = [UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT]
const cardinals = [UP, RIGHT, DOWN, LEFT]

var modifier_Key_States : Dictionary = { } #Should be used for modifier key combinations
var CORNER2TILE: Dictionary = {UP_LEFT: {full_grass: grass_se, full_soil: soil_se, 
						grass_se: SENTINEL_VEC, soil_se: SENTINEL_VEC},
					UP_RIGHT: {full_grass: grass_sw, full_soil: soil_sw, 
						grass_sw: SENTINEL_VEC, soil_sw: SENTINEL_VEC},
					DOWN_RIGHT: {full_grass: grass_nw, full_soil: soil_nw, 
						grass_nw: SENTINEL_VEC, soil_nw: SENTINEL_VEC},
					DOWN_LEFT: {full_grass: grass_ne, full_soil: soil_ne, 
						grass_ne: SENTINEL_VEC, soil_ne: SENTINEL_VEC}}

#for corner in CORNER2TILE:
#	entry = CORNER2TILE[corner]
#	for cell_type in cell_types:
#		pass

const BEAM_START = {UP_LEFT: Vector2(2, 1), UP: Vector2(0, 0), UP_RIGHT: Vector2(3, 1),
					RIGHT: Vector2(1, 0), DOWN_RIGHT: Vector2(2, 1), DOWN: Vector2(2, 0),
					DOWN_LEFT: Vector2(3, 1), LEFT: Vector2(3, 0)}
const BEAM_CONTINUE = {UP_LEFT: Vector2(2, 1), UP: Vector2(0, 1), UP_RIGHT: Vector2(3, 1),
					RIGHT: Vector2(1, 1), DOWN_RIGHT: Vector2(2, 1), DOWN: Vector2(0, 1),
					DOWN_LEFT: Vector2(3, 1), LEFT: Vector2(1, 1)}
const BEAM_SPEED = 0.5

const destr_particles = preload("res://game/destructors/CPUParticles2D.tscn")

const bomb_sfx = preload("res://game/sounds/bomb.wav")
const death_sfx = preload("res://game/sounds/death.wav")
const fire_sfx = preload("res://game/sounds/fire.wav")
const lazer_sfx = preload("res://game/sounds/lazer.wav")
const bb_sfx = preload("res://game/sounds/beam_bomb.wav")

# TODO: be able to use PoolVector2Array
var fire_cells: Array = []

func destructor(dir, loc=null):
	if loc == null:
		loc = world_to_map(position)
	if get_cellv(loc + dir) != -1:
		var parts = destr_particles.instance()
		parts.position = map_to_world(loc + dir)
		add_child(parts)
	set_cellv(loc + dir, -1)

func carver(dir, loc=null):
	if loc == null:
		loc = world_to_map(position)
	var dest = loc + dir
	if get_cellv(dest) == -1:
		return
	var orig_type = get_cell_autotile_coord(dest.x, dest.y)
	print("carver dest", dest)
	print("carver type", orig_type)
	var new_type = Vector2.ZERO
	var clear_tile = false

	# up right or down left is ne/sw
	if dir == UP_RIGHT or dir == DOWN_LEFT:
		if orig_type == full_grass:
			new_type = grass_se
		elif orig_type == full_soil:
			new_type = soil_se
		elif orig_type == grass_ne or orig_type == grass_sw or orig_type == soil_ne or orig_type == soil_sw:
			clear_tile = true

	# up left or down right is nw/se
	if dir == UP_LEFT or dir == DOWN_RIGHT:
		if orig_type == full_grass:
			new_type = grass_sw
		elif orig_type == full_soil:
			new_type = soil_sw
		elif orig_type == grass_se or orig_type == grass_nw or orig_type == soil_se or orig_type == soil_nw:
			clear_tile = true

	if clear_tile:
		if get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
		set_cellv(dest, -1)
	elif new_type != Vector2.ZERO:
		set_cell(dest.x, dest.y, 0, false, false, false, new_type)

func diag_cut(corner, dest):
	# corner is the corner to delete
	var orig_type = get_cell_autotile_coord(dest.x, dest.y)
	print("diag_cut dest", dest)
	print("diag_cut type", orig_type)
	var entry = CORNER2TILE[corner]
	if orig_type in entry:
		if entry[orig_type] == SENTINEL_VEC:
			print("sentinel. orig:", orig_type)
			pass
		else:
			print(orig_type, "setting cell to", entry[orig_type])
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
			set_cell(dest.x, dest.y, 0, false, false, false, entry[orig_type])
	else:
		print("clear the tile from corner", corner)
		if get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
		set_cellv(dest, -1)
		print("cleared tile is", get_cellv(dest))

func _on_start_cut(dir, loc):
	if dir in diagonals:
		carver(dir, loc)
	else:
		destructor(dir, loc)

func _on_start_bomb(loc):
	Sfx.stream = bomb_sfx
	Sfx.play()
	$VFXTileMap.set_cellv(loc, 1)
	var new_loc
	var i
	var bomb_time = 0.2
	for f in [2, 3]:
		i = 0
		yield(get_tree().create_timer(bomb_time), "timeout")
		for col in range(3):
			for row in range(3):
				if row == 1 and col == 1:
					new_loc = loc
				else:
					new_loc = loc + circle_dirs[i]
					i += 1
				$VFXTileMap.set_cell(new_loc.x, new_loc.y, f, false, false, false, Vector2(col, row))
	yield(get_tree().create_timer(bomb_time), "timeout")
	for dir in circle_dirs:
		destructor(dir, loc)
	# clean up vfx
	i = 0
	for col in range(3):
		for row in range(3):
			if row == 1 and col == 1:
				new_loc = loc
			else:
				new_loc = loc + circle_dirs[i]
				i += 1
			$VFXTileMap.set_cellv(new_loc, -1)

func _on_start_beam(dir, loc):
	Sfx.stream = lazer_sfx
	Sfx.play()
	var dest = loc + dir
	var dest_cell = get_cellv(dest)
	var start = true
	var cut_y = Vector2(0, dir.y)
	var cut_x = Vector2(dir.x, 0)
	var corner_x = Vector2(dir.x*-1, dir.y)
	var corner_y = Vector2(dir.x, dir.y*-1)
	var vfx_tiles : PoolVector2Array = []
	var tile
	while dest_cell != -1:
		if start:
			tile = BEAM_START[dir]
			start = false
		else:
			tile = BEAM_CONTINUE[dir]
		$VFXTileMap.set_cell(dest.x, dest.y, 0, false, false, false, tile)
		vfx_tiles.append(dest)
		yield(get_tree().create_timer(BEAM_SPEED), "timeout")
		destructor(dir, loc)
		if dir in diagonals:
			# we need to carve in the direction dir, so we need to start from the tile to be cut minus dir
#			carver(dir, dest + cut_y - dir)
#			carver(-dir, dest + cut_x + dir)
			print("x: ", dest + cut_x, " y: ", dest + cut_y)
			
			diag_cut(corner_x, dest + cut_x)
			diag_cut(corner_y, dest + cut_y)
		loc = dest
		dest = loc + dir
		dest_cell = get_cellv(dest)
	yield(get_tree().create_timer(BEAM_SPEED), "timeout")
	for tile in vfx_tiles:
		$VFXTileMap.set_cellv(tile, -1)
	return loc

func _on_start_fire(dest):
	Sfx.stream = fire_sfx
	Sfx.play()
	fire_cells.append(dest)
	$VFXTileMap.set_cellv(dest, 4)
	if $FireTimer.is_stopped():
		$FireTimer.start()

func _on_spread_fire():
	var new_fire_cells: Array = []
	var dest
	var cell_type
	for src in fire_cells:
		destructor(src)
		$VFXTileMap.set_cellv(src, -1)
	for src in fire_cells:
		for dir in cardinals:
			dest = src + dir
			cell_type = get_cellv(dest)
			if cell_type == -1:
				continue
			# TODO: the second find call may be superfluous with the earlier call to destruction
			if new_fire_cells.find(dest) == -1 and fire_cells.find(dest) == -1:
				new_fire_cells.append(dest)
				$VFXTileMap.set_cellv(dest, 4)
	fire_cells = new_fire_cells
	if new_fire_cells.empty():
		$FireTimer.stop()

func _on_start_beam_bomb(dir, loc):
	Sfx.stream = bb_sfx
	Sfx.play()
	var dest = loc + dir
	var dest_cell = get_cellv(dest)
	var start = true
	var tile
	var vfx_tiles : PoolVector2Array = []
	while dest_cell != -1:
		if start:
			tile = BEAM_START[dir]
			start = false
		else:
			tile = BEAM_CONTINUE[dir]
		$VFXTileMap.set_cell(dest.x, dest.y, 0, false, false, false, tile)
		vfx_tiles.append(dest)
		yield(get_tree().create_timer(BEAM_SPEED), "timeout")
		_on_start_cut(dir, loc)
		loc = dest
		dest = loc + dir
		dest_cell = get_cellv(dest)
	yield(get_tree().create_timer(BEAM_SPEED), "timeout")
#	if not loc is Vector2:
#		loc = loc.resume()
	for tile in vfx_tiles:
		$VFXTileMap.set_cellv(tile, -1)
	_on_start_bomb(dest)


# Called when the node enters the scene tree for the first time.
func _ready():
	LevelSaver.level_scene = own_path
	$Wizard.connect("start_fire", self, "_on_start_fire")
	$Wizard.connect("start_cut", self, "_on_start_cut")
	$Wizard.connect("start_bomb", self, "_on_start_bomb")
	$Wizard.connect("start_beam", self, "_on_start_beam")
	$Wizard.connect("start_beam_bomb", self, "_on_start_beam_bomb")
	$FireTimer.connect("timeout", self, "_on_spread_fire")

func _input(event):
	if event.is_action_pressed("tutorial"):
		get_tree().change_scene("res://game/tutorial/tutorial_move.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	print($Wizard.position)

func bomb():
	return $Wizard/WeaponsGUI/TbMid.active

func lazer():
	return $Wizard/WeaponsGUI/TbTop.active

func fire():
	return $Wizard/WeaponsGUI/TbBot.active

func cut():
	return false

func _on_Deathplane_body_entered(body):
	print(body, "Has gone to the death plane")
	if body.get_collision_layer_bit(1):
		Sfx.stream = death_sfx
		Sfx.play()
		get_tree().reload_current_scene()
	else:
		body.queue_free()
