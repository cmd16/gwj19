extends Node

var timestamp_class = preload("res://game/scenes/Timestamp.gd")

export var music_volume : int = 50
export var sound_volume : int = 75
export var wizard_spawn : Vector2 = Vector2(52, 40)
export var music_enabled = true
export var sound_enabled = true
export var updateSoundSettings = false
export var minimap_image : Image
export var plot_player_position : Vector2 = Vector2(52, 40)
var activeBomb : bool = false
var activeLazer : bool = false
var activeCut : bool = false
var activeFire : bool = false
var activeBeam : bool = false
var tool_events : Dictionary = { }
var time_tool_check = timestamp_class.timestamp.new()
var minimap_check = timestamp_class.timestamp.new()
var timer : Timer

const SOIL_COLOR : Color = Color(0.62, 0.47, 0.25)
signal sound_update

func _globtimer_callback():
	#print('_globtimer_callback\n')
	if updateSoundSettings:
		emit_signal("sound_update")
		updateSoundSettings = false

func create_mini_map(tileMap : TileMap, width : int, height : int):
	minimap_image = Image.new()
	minimap_image.create(width, height, false, Image.FORMAT_RGBA8)
	minimap_image.fill(Color.transparent)
	minimap_image.lock()
	var cell_value
	for xi in range(0, width):
		for yi in range(0, height):
			cell_value = tileMap.get_cell(xi, yi)
			if cell_value >= 0:
				minimap_image.set_pixelv(Vector2(xi, yi), SOIL_COLOR)
	minimap_image.unlock()
	minimap_image.shrink_x2()
	minimap_image.save_png("res://dynamic/minimap.png")

func _ready():
	timer = Timer.new()
	timer.set_one_shot(false)
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_IDLE)
	timer.set_wait_time(0.25)
	timer.connect("timeout", self, "_globtimer_callback")
	add_child(timer)
	timer.start()
	minimap_check.reset_time()
	print('global _ready()\n')

func setMusicAndSound(musicEnable, soundEnable, musVolune, sndVolume):
	music_enabled = musicEnable
	sound_enabled = soundEnable

func setMinimapUpdate():
	minimap_check.reset_time()

func updatedMiniMap() -> bool:
	var res = false
	var event_time = timestamp_class.timestamp.new()
	event_time.reset_time()
	var ticksElapsed = event_time.subtract_ticks(minimap_check)
	if ticksElapsed <= 300:
		res = true
	return res

func activate_tool(tool_name, dir, loc, event_time):
	if tool_events.keys().size() >= 3:
		tool_events.clear()
	if tool_events.has(tool_name):
		tool_events.erase(tool_name)
	tool_events[tool_name] = [dir, loc, event_time]

func getToolEventFields(tool_name, clear):
	if tool_events.has(tool_name):
		var res = tool_events[tool_name]
		if clear:
			tool_events.erase(tool_name)
		return res
	return null

func getActiveTool():
	time_tool_check.reset_time()
	var loopAgain = true
	while loopAgain:
		loopAgain = false
		for key in tool_events.keys():
			var event_value = tool_events[key]
			var event_time = event_value[2]
			var ticksElapsed = time_tool_check.subtract_ticks(event_time)
			if ticksElapsed >= 800:
				tool_events.erase(key)
				loopAgain = true
			print('getActiveTool, tool=%s\n' % [key])
			print('getActiveTool, time_tool_check=%s, time-diff=%d\n' % [time_tool_check.quickprint(), ticksElapsed])
			if ticksElapsed <= 500:
				return key
	return ""

func switchOffAllTools():
	activeBeam = false
	activeBomb = false
	#activeLazer = false
	activeCut = false
	activeFire = false
	print('switchOffAllTools\n')

func setBeam(active):
	activeBeam = active
	print('setBeam, active=%s\n' % [active])

func setBomb(active):
	activeBomb = active
	print('setBomb, active=%s\n' % [active])

func setCut(active):
	activeCut = active
	print('setCut, active=%s\n' % [active])

func setFire(active):
	activeFire = active
	print('setFire, active=%s\n' % [active])

func isBeam():
	return activeBeam

func isBomb():
	return activeBomb

func isCut():
	return activeCut
	
func isFire():
	return activeFire
