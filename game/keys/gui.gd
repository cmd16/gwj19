extends HBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func add(s, c):
	get_node(s).modulate = Color(c)

var alpha = false
var beta = false
var gamma = false
var delta = false
var epsilon = false

func add_alpha():
	alpha = true
	add("Alpha", "bbff00")

func add_beta():
	beta = true
	add("Beta", "bbff00")

func add_gamma():
	gamma = true
	add("Gamma", "bbff00")

func add_delta():
	delta = true
	add("Delta", "bbff00")

func add_epsilon():
	epsilon = true
	add("Epsilon", "bbff00")
