extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal level_completed()

var count: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if count >= 5:
		texture = load("res://game/keys/full.tres")
		$Entry.monitoring = true


func add_alpha():
	count += 1
	$Alpha.show()


func _on_Beta_collected():
	count += 1
	$Beta.show()


func _on_Gamma_collected():
	count += 1
	$Gamma.show()


func _on_Delta_collected():
	count += 1
	$Delta.show()


func _on_Epsilon_collected():
	count += 1
	$Epsilon.show()

func _on_Entry_body_entered(body):
	if body.get_collision_layer_bit(1):
		emit_signal("level_completed")
#		get_tree().change_scene("res://game/keys/win.tscn")
