extends Panel

var draw_update = false
var imgTexture : ImageTexture
var playX = 1
var playY = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	imgTexture = ImageTexture.new()

func plot_player_position(x : int, y : int):
	playX = x
	playY = y

func loadImage(image_resource): # "res://dynamic/minimap.png"
	imgTexture.load(image_resource)

func _draw():
	if (draw_update):
		draw_update = false
		draw_rect(Rect2(8, 8, 3, 3), Color.rosybrown, true)
		var angle_from = 0
		var angle_to = 180
		var center = Vector2(200, 200)
		var radius = 80
		var dcolor = Color(1.0, 0.0, 0.0)
		if imgTexture != null:
			draw_texture(imgTexture, Vector2(2, 3))
		draw_circle(Vector2(playX, playY), 1.0, Color(0.425, 0.375, 0.8125))
		#draw_circle_arc(center, radius, angle_from, angle_to, dcolor )
		print("_draw() , draw player")


func _on_DrawTimer_timeout():
	draw_update = Globals.updatedMiniMap()
	var playpos = Globals.plot_player_position
	plot_player_position(playpos.x, playpos.y)
	if draw_update:
		loadImage("res://dynamic/minimap.png")
	draw_update = true
	update()
