extends VBoxContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	var is_beam = event.is_action_pressed("beam")
	var is_bomb = event.is_action_pressed("bomb")
	var is_fire = event.is_action_pressed("fire")
	var is_cut = event.is_action_pressed("cut")
	if is_beam and not ($Tb3.active):
		$Tb2._pressed()
	elif is_bomb and not ($Tb3.active):
		$Tb1._pressed()
	elif is_fire and not ($Tb1.active or $Tb2.active or $Tb4.active):
		$Tb3._pressed()
	elif is_cut and not ($Tb3.active):
		$Tb4._pressed()
	elif is_beam or is_bomb or is_fire or is_cut:
		$AudioStreamPlayer.play()  # play the "bad" sound so you know that's invalid

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
