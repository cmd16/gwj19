extends TextureButton


# Declare member variables here. Examples:
var active = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _pressed():
	active = not active

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if active:
		modulate = Color(1, 0.2, 0)
	else:
		modulate = Color(0, 0.055, .31)
