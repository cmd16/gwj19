path = "/Users/cat/GodotProjects/gwj19_copy/gwj19/proc-gen/pre-generated/"
name = path + "procgenmap_24032020#001827_in#2.map"
in_name = path + name
in_name = "/Users/cat/GodotProjects/gwj19_copy/gwj19/proc-gen/pre-generated/procgenmap_24032020#001827_in#2.map"
out_name = "/Users/cat/GodotProjects/gwj19_copy/gwj19/proc-gen/pre-generated/procgenmap_24032020#001827_in#2.map_py.txt"

f_in = open(in_name)
n_columns = int(next(f_in))
n_rows = int(next(f_in))
my_tiles = []
soil_tiles = []
grass_tiles = []
r = 0
surface_cols = []
for row in f_in:
    my_tiles.append(row.split())
    for c in range(n_columns):
        cell = int(my_tiles[-1][c])
        if cell == 0:
            my_tiles[-1][c] = -1
            try:
                surface_cols.remove(c)
            except ValueError:
                pass
        elif c in surface_cols:
            # soil
            my_tiles[-1][c] = 'Vector2(0,1)'
            soil_tiles.append((r, c))
        else:
            # grass
            my_tiles[-1][c] = 'Vector2(0,0)'
            surface_cols.append(c)
            grass_tiles.append((r, c))
    r += 1
    
f_in.close()

print(grass_tiles)
print(soil_tiles)

# f_out = open(out_name, 'w')

# f_out.write(str(my_tiles).replace("'", "") + '\n')
# print(my_tiles)

