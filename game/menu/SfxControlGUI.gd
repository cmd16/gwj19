extends HBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_TextureButton_toggled(button_pressed):
	if button_pressed:
		$HSlider.set_value(0)
		Globals.sound_enabled = !$TextureButton.pressed
	Globals.sound_volume = $HSlider.value
	$HSlider.set_editable(not button_pressed)
	Globals.updateSoundSettings = true

func _on_HSlider_value_changed(value):
	Globals.sound_volume = value
	Globals.updateSoundSettings = true
