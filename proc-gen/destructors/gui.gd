extends VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("bomb") or event.is_action_pressed("beam") or \
	event.is_action_pressed("fire") or event.is_action_pressed("cut"):
		Globals.switchOffAllTools()
	if event.is_action_pressed("bomb") and not ($Tb3.active or $Tb4.active):
		$Tb1._pressed()
		Globals.setBomb($Tb1.active)
		#if !$Tb1.active:
		#	Globals.setBomb($Tb2.active)
		#else:
		#	Globals.setBomb($Tb2.active) #beambomb
	elif event.is_action_pressed("beam") and not ($Tb3.active or $Tb4.active):
		$Tb2._pressed()
		Globals.setBeam($Tb2.active)
		#if !$Tb1.active:
		#	Globals.setBeam($Tb1.active)
		#else:
		#	Globals.setBeam($Tb1.active) #beambomb
	elif event.is_action_pressed("fire") and not ($Tb1.active or $Tb2.active):
		$Tb3._pressed()
		Globals.setFire(true)
	elif event.is_action_pressed("cut") and not ($Tb2.active or $Tb3.active):
		$Tb4._pressed()
		Globals.setCut(true)
	elif event.is_action_pressed("beam") or event.is_action_pressed("bomb") or event.is_action_pressed("fire"):
		$AudioStreamPlayer.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
