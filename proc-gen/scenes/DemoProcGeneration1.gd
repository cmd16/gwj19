extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$MapLevelProcedural._ready()
	$MapLevelProcedural.setdimension(80, 60)
	$MapLevelProcedural.generatelevel("maplevel1.map")
	$MapLevelProcedural.dumpdebug("datafile")
	$MapLevelProcedural.cleardebug("datafile")
	$MapLevelProcedural.generateautolevel()
	$MapLevelProcedural.autoleveldatamap("gencavern1.map")

func placePortal():
	pass
