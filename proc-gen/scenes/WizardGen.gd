#WizardGen

#The modified version that is adapted to work with the "levelgenerationA" scene
extends KinematicBody2D

var timestamp_class = preload("res://game/scenes/Timestamp.gd")

export (int) var speed = 1200
export (int) var jump_speed = -1800
export (int) var gravity = 4000
export (float) var jump_release = 0.1

signal start_fire(src)
signal start_cut(dir, loc)
signal start_bomb(loc)
signal start_beam(dir, loc)
signal start_beam_bomb(dir, loc)

const MOVE_NONE = Vector2(0, 0)
const UP_LEFT = Vector2(-1, -1)
const UP = Vector2(0, -1)
const UP_RIGHT = Vector2(1, -1)
const RIGHT = Vector2(1, 0)
const DOWN_RIGHT = Vector2(1, 1)
const DOWN = Vector2(0, 1)
const DOWN_LEFT = Vector2(-1, 1)
const LEFT = Vector2(-1, 0)
const circle_dirs = [UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT]
const diagonals = [UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT]
const cardinals = [UP, RIGHT, DOWN, LEFT]

var velocity = Vector2.ZERO
var canMove = true
var key_dir = MOVE_NONE
var time_tool_dirpressed = timestamp_class.timestamp.new()

func cut(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	#emit_signal("start_cut", dir, loc)
	#doing a global "bypass" hack
	var current_game_time = timestamp_class.timestamp.new()
	current_game_time.reset_time()
	Globals.activate_tool("start_cut", dir, loc, current_game_time)

func bomb(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_bomb", loc)
	var current_game_time = timestamp_class.timestamp.new()
	current_game_time.reset_time()
	Globals.activate_tool("start_bomb", dir, loc, current_game_time)

func beam(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_beam", dir, loc)
	var current_game_time = timestamp_class.timestamp.new()
	current_game_time.reset_time()
	Globals.activate_tool("start_beam", dir, loc, current_game_time)

func bomb_beam(dir, loc=null):
	# NOT USED, TOO CHAOTIC
	if loc == null:
		loc = get_parent().world_to_map(position)
	bomb(loc)
	for dir in cardinals:
		beam(dir, loc + dir)

func beam_bomb(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	emit_signal("start_beam_bomb", dir, loc)
	var current_game_time = timestamp_class.timestamp.new()
	current_game_time.reset_time()
	Globals.activate_tool("start_beam_bomb", dir, loc, current_game_time)

func fire(dir, loc=null):
	if loc == null:
		loc = get_parent().world_to_map(position)
	var dest = loc + dir
	emit_signal("start_fire", dest)
	var current_game_time = timestamp_class.timestamp.new()
	current_game_time.reset_time()
	Globals.activate_tool("start_fire", dir, loc, current_game_time)

func getallnodes(node):
	for N in node.get_children():
		if N.get_child_count() > 0:
			print("["+N.get_name()+"]")
			getallnodes(N)
		else:
			# Do something
			print("- "+N.get_name())

func selectToolAction(dir):
	var levelGenerateNode = get_parent()
	if levelGenerateNode == null:
		return
	#print('selectToolAction, levelGenerateNode=%s' % [levelGenerateNode])
	#if levelGenerateNode.bomb() and not levelGenerateNode.lazer() and Input.is_action_just_pressed("attack"):
	#	bomb(dir)
	if Globals.isBomb() and not Globals.isBeam():
		bomb(dir)
		return
	if Globals.isBeam() and dir != MOVE_NONE:
		if Globals.isBomb():
			beam_bomb(dir)
			print("beam-bombing")
			return
		else:
			beam(dir)
			print("beaming", dir)
			return
	if Globals.isFire() and dir != MOVE_NONE:
		fire(dir)
		print("firing")
		return
	if Globals.isCut() and dir != MOVE_NONE:
		cut(dir)
		print("cutting")
		return

func get_input():
	velocity.x = 0
	var current_time = timestamp_class.timestamp.new()
	current_time.reset_time()
	var ticksElapsed = current_time.subtract_ticks(time_tool_dirpressed)
	if ticksElapsed > 500:
		key_dir = MOVE_NONE
	
	if true:
		var left = Input.is_action_pressed("left")
		var right = Input.is_action_pressed("right")
		var up = Input.is_action_pressed("up")
		var down = Input.is_action_pressed("down")
		
		if left:
			$Sprite.scale = Vector2(1, 1)
		elif right:
			$Sprite.scale = Vector2(-1, 1)
		
		var just_left = Input.is_action_just_pressed("left")
		var just_right = Input.is_action_just_pressed("right")
		var just_up = Input.is_action_just_pressed("up")
		var just_down = Input.is_action_just_pressed("down")
		if (just_left or just_right or just_up or just_down):
			time_tool_dirpressed.reset_time()

		if just_up:
			if just_left:
				key_dir = UP_LEFT
			elif just_right:
				key_dir = UP_RIGHT
			else:
				key_dir = UP
		elif just_down:
			if just_left:
				key_dir = DOWN_LEFT
			elif just_right:
				key_dir = DOWN_RIGHT
			else:
				key_dir = DOWN
		elif just_left:
			key_dir = LEFT
		elif just_right:
			key_dir = RIGHT
		
		if Input.is_action_pressed("right"):
			if canMove:
				velocity.x += speed
		elif Input.is_action_pressed("left"):
			if canMove:
				velocity.x -= speed
		if Input.is_action_just_pressed("attack"):
			canMove = false
			selectToolAction(key_dir)
		if Input.is_action_just_released("attack"):
			canMove = true

func _physics_process(delta):
	get_input()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			velocity.y = -jump_speed
	if Input.is_action_just_released("jump"):
		if velocity.y < 0:
			velocity.y *= jump_release

func _input(event):
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	time_tool_dirpressed.reset_time()
	position = Globals.wizard_spawn

