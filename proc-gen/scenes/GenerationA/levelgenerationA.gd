extends TileMap

export (PackedScene) var AnimatedBombSprite
export var own_path = "res://scenes/GenerationA/levelgenerationA.tscn"

const FULL_GRASS = Vector2(0, 0)
const GRASS_SW = Vector2(3, 0)
const GRASS_SE = Vector2(4, 0)
const GRASS_NE = Vector2(5, 0)
const GRASS_NW = Vector2(6, 0)
const FULL_SOIL = Vector2(0, 1)
const SOIL_SW = Vector2(3, 1)
const SOIL_SE = Vector2(4, 1)
const SOIL_NE = Vector2(5, 1)
const SOIL_NW = Vector2(6, 1)
const CELL_TYPES = [FULL_GRASS, GRASS_SW, GRASS_SE, GRASS_NE, GRASS_NW, 
					FULL_SOIL, SOIL_SW, SOIL_SE, SOIL_NE, SOIL_NW]
const SENTINEL_VEC = Vector2.INF
const CLEAR_VEC = Vector2(-1, -1)

const UP_LEFT = Vector2(-1, -1)
const UP = Vector2(0, -1)
const UP_RIGHT = Vector2(1, -1)
const RIGHT = Vector2(1, 0)
const DOWN_RIGHT = Vector2(1, 1)
const DOWN = Vector2(0, 1)
const DOWN_LEFT = Vector2(-1, 1)
const LEFT = Vector2(-1, 0)
const CIRCLE_DIRS = [UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT]
const DIAGONALS = [UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT]
const CARDINALS = [UP, RIGHT, DOWN, LEFT]
var CORNER2TILE: Dictionary = {UP_LEFT: {FULL_GRASS: GRASS_SE, FULL_SOIL: SOIL_SE, 
						GRASS_SE: SENTINEL_VEC, SOIL_SE: SENTINEL_VEC},
					UP_RIGHT: {FULL_GRASS: GRASS_SW, FULL_SOIL: SOIL_SW, 
						GRASS_SW: SENTINEL_VEC, SOIL_SW: SENTINEL_VEC},
					DOWN_RIGHT: {FULL_GRASS: GRASS_NW, FULL_SOIL: SOIL_NW, 
						GRASS_NW: SENTINEL_VEC, SOIL_NW: SENTINEL_VEC},
					DOWN_LEFT: {FULL_GRASS: GRASS_NE, FULL_SOIL: SOIL_NE, 
						GRASS_NE: SENTINEL_VEC, SOIL_NE: SENTINEL_VEC}}


const BEAM_START = {UP_LEFT: Vector2(0, 2), UP: Vector2(0, 0), UP_RIGHT: Vector2(1, 2),
					RIGHT: Vector2(1, 0), DOWN_RIGHT: Vector2(2, 2), DOWN: Vector2(2, 0),
					DOWN_LEFT: Vector2(3, 2), LEFT: Vector2(3, 0)}
const BEAM_CONTINUE = {UP_LEFT: Vector2(2, 1), UP: Vector2(0, 1), UP_RIGHT: Vector2(3, 1),
					RIGHT: Vector2(1, 1), DOWN_RIGHT: Vector2(2, 1), DOWN: Vector2(0, 1),
					DOWN_LEFT: Vector2(3, 1), LEFT: Vector2(1, 1)}
const BEAM_CORNER = {UP_LEFT: Vector2(0, 3), UP_RIGHT: Vector2(1, 3), 
					DOWN_RIGHT: Vector2(2, 3), DOWN_LEFT: Vector2(3, 3)}
const BEAM_SPEED = 0.5
const BEAM_TILE = 5

const destr_particles = preload("res://destructors/CPUParticles2D.tscn")

const bomb_sfx = preload("res://sounds/bomb.wav")
const death_sfx = preload("res://sounds/death.wav")
const fire_sfx = preload("res://sounds/fire.wav")
const lazer_sfx = preload("res://sounds/lazer.wav")
const bb_sfx = preload("res://sounds/beam_bomb.wav")

# TODO: be able to use PoolVector2Array
var fire_cells: Array = []

func destructor(dir, loc=null):
	if loc == null:
		loc = world_to_map(position)
	if get_cellv(loc + dir) != -1:
		var parts = destr_particles.instance()
		parts.position = map_to_world(loc + dir)
		add_child(parts)
	set_cellv(loc + dir, -1)

func carver(dir, loc=null):
	if loc == null:
		loc = world_to_map(position)
	var dest = loc + dir
	if get_cellv(dest) == -1:
		return
	var orig_type = get_cell_autotile_coord(dest.x, dest.y)
	print("carver dest", dest)
	print("carver type", orig_type)
	var new_type = Vector2.ZERO
	var clear_tile = false

	# up right or down left is ne/sw
	if dir == UP_RIGHT or dir == DOWN_LEFT:
		if orig_type == FULL_GRASS:
			new_type = GRASS_SE
		elif orig_type == FULL_SOIL:
			new_type = SOIL_SE
		elif orig_type == GRASS_NE or orig_type == GRASS_SW or orig_type == SOIL_NE or orig_type == SOIL_SW:
			clear_tile = true

	# up left or down right is nw/se
	if dir == UP_LEFT or dir == DOWN_RIGHT:
		if orig_type == FULL_GRASS:
			new_type = GRASS_SW
		elif orig_type == FULL_SOIL:
			new_type = SOIL_SW
		elif orig_type == GRASS_SE or orig_type == GRASS_NW or orig_type == SOIL_SE or orig_type == SOIL_NW:
			clear_tile = true

	if clear_tile:
		if get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
		set_cellv(dest, -1)
	elif new_type != Vector2.ZERO:
		set_cell(dest.x, dest.y, 0, false, false, false, new_type)

func diag_cut(corner, dest):
	# corner is the corner to delete
	if get_cellv(dest) == -1:
		# don't cut an empty tile
		return
	var orig_type = get_cell_autotile_coord(dest.x, dest.y)
	var entry = CORNER2TILE[corner]
	if orig_type in entry:
		if entry[orig_type] == SENTINEL_VEC:
			pass
		else:
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
			set_cell(dest.x, dest.y, 0, false, false, false, entry[orig_type])
	else:
		if get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = map_to_world(dest)
			add_child(parts)
		set_cellv(dest, -1)
		print("cleared tile is", get_cellv(dest))

func _on_start_cut(dir, loc):
#	if dir in DIAGONALS:
#		carver(dir, loc)
#	else:
	destructor(dir, loc)

func _on_start_bomb(loc):
	Sfx.stream = bomb_sfx
	Sfx.play()
	$VFXTileMap.set_cellv(loc, 1)
	var new_loc
	var i
	var bomb_time = 0.2
	for f in [2, 3]:
		i = 0
		yield(get_tree().create_timer(bomb_time), "timeout")
		for col in range(3):
			for row in range(3):
				if row == 1 and col == 1:
					new_loc = loc
				else:
					new_loc = loc + CIRCLE_DIRS[i]
					i += 1
				$VFXTileMap.set_cell(new_loc.x, new_loc.y, f, false, false, false, Vector2(col, row))
	yield(get_tree().create_timer(bomb_time), "timeout")
	for dir in CIRCLE_DIRS:
		destructor(dir, loc)
	# clean up vfx
	i = 0
	for col in range(3):
		for row in range(3):
			if row == 1 and col == 1:
				new_loc = loc
			else:
				new_loc = loc + CIRCLE_DIRS[i]
				i += 1
			$VFXTileMap.set_cellv(new_loc, -1)

func _on_start_beam(dir, loc):
	Sfx.stream = lazer_sfx
	Sfx.play()
	var dest = loc + dir
	var dest_cell = get_cellv(dest)
	var start = true
	var cut_y = Vector2(0, dir.y)
	var cut_x = Vector2(dir.x, 0)
	var corner_x
	var corner_y
	var corner_x_vfx
	var corner_y_vfx
	if dir in DIAGONALS:
		corner_x = Vector2(dir.x*-1, dir.y)
		corner_y = Vector2(dir.x, dir.y*-1)
		corner_x_vfx = BEAM_CORNER[corner_x]
		corner_y_vfx = BEAM_CORNER[corner_y]
	var vfx_tiles = []
	var tile
	var x_dest
	var y_dest
	while dest_cell != -1:
		if start:
			tile = BEAM_START[dir]
			start = false
		else:
			tile = BEAM_CONTINUE[dir]
		print("tile", tile)
		$VFXTileMap.set_cell(dest.x, dest.y, BEAM_TILE, false, false, false, tile)
		vfx_tiles.append(dest)
		yield(get_tree().create_timer(BEAM_SPEED), "timeout")
		destructor(dir, loc)
		if dir in DIAGONALS:
			x_dest = dest + cut_x
			y_dest = dest + cut_y
			$VFXTileMap.set_cell(x_dest.x, x_dest.y, BEAM_TILE, false, false, false, corner_x_vfx)
			vfx_tiles.append(x_dest)
			$VFXTileMap.set_cell(y_dest.x, y_dest.y, BEAM_TILE, false, false, false, corner_y_vfx)
			vfx_tiles.append(y_dest)
			diag_cut(corner_x, x_dest)
			diag_cut(corner_y, y_dest)
		loc = dest
		dest = loc + dir
		dest_cell = get_cellv(dest)
	if dir in DIAGONALS:  # remove the last two "corner" VFX tiles
		for i in range(2):
			$VFXTileMap.set_cellv(vfx_tiles.pop_back(), -1)
	yield(get_tree().create_timer(BEAM_SPEED), "timeout")
	for tile in vfx_tiles:
		$VFXTileMap.set_cellv(tile, -1)
	return loc

func _on_start_fire(dest):
	Sfx.stream = fire_sfx
	Sfx.play()
	fire_cells.append(dest)
	$VFXTileMap.set_cellv(dest, 4)
	if $FireTimer.is_stopped():
		$FireTimer.start()

func _on_spread_fire():
	var new_fire_cells: Array = []
	var dest
	var cell_type
	for src in fire_cells:
		destructor(src)
		$VFXTileMap.set_cellv(src, -1)
	for src in fire_cells:
		for dir in CARDINALS:
			dest = src + dir
			cell_type = get_cellv(dest)
			if cell_type == -1:
				continue
			# TODO: the second find call may be superfluous with the earlier call to destruction
			if new_fire_cells.find(dest) == -1 and fire_cells.find(dest) == -1:
				new_fire_cells.append(dest)
				$VFXTileMap.set_cellv(dest, 4)
	fire_cells = new_fire_cells
	if new_fire_cells.empty():
		$FireTimer.stop()

func _on_start_beam_bomb(dir, loc):
	Sfx.stream = lazer_sfx
	Sfx.play()
	var dest = loc + dir
	var dest_cell = get_cellv(dest)
	var start = true
	var cut_y = Vector2(0, dir.y)
	var cut_x = Vector2(dir.x, 0)
	var corner_x
	var corner_y
	var corner_x_vfx
	var corner_y_vfx
	if dir in DIAGONALS:
		corner_x = Vector2(dir.x*-1, dir.y)
		corner_y = Vector2(dir.x, dir.y*-1)
		corner_x_vfx = BEAM_CORNER[corner_x]
		corner_y_vfx = BEAM_CORNER[corner_y]
	var vfx_tiles = []
	var tile
	var x_dest
	var y_dest
	while dest_cell != -1:
		if start:
			tile = BEAM_START[dir]
			start = false
		else:
			tile = BEAM_CONTINUE[dir]
		print("tile", tile)
		$VFXTileMap.set_cell(dest.x, dest.y, BEAM_TILE, false, false, false, tile)
		vfx_tiles.append(dest)
		yield(get_tree().create_timer(BEAM_SPEED), "timeout")
		destructor(dir, loc)
		if dir in DIAGONALS:
			x_dest = dest + cut_x
			y_dest = dest + cut_y
			$VFXTileMap.set_cell(x_dest.x, x_dest.y, BEAM_TILE, false, false, false, corner_x_vfx)
			vfx_tiles.append(x_dest)
			$VFXTileMap.set_cell(y_dest.x, y_dest.y, BEAM_TILE, false, false, false, corner_y_vfx)
			vfx_tiles.append(y_dest)
			diag_cut(corner_x, x_dest)
			diag_cut(corner_y, y_dest)
		loc = dest
		dest = loc + dir
		dest_cell = get_cellv(dest)
#	if dir in DIAGONALS:  # remove the last two "corner" VFX tiles
#		for i in range(2):
#			$VFXTileMap.set_cellv(vfx_tiles.pop_back(), -1)
	yield(get_tree().create_timer(BEAM_SPEED / 3), "timeout")
	for tile in vfx_tiles:
		$VFXTileMap.set_cellv(tile, -1)

	_on_start_bomb(dest)


# Called when the node enters the scene tree for the first time.
func _ready():
	LevelSaver.level_scene = own_path
	$Wizard.connect("start_fire", self, "_on_start_fire")
	$Wizard.connect("start_cut", self, "_on_start_cut")
	$Wizard.connect("start_bomb", self, "_on_start_bomb")
	$Wizard.connect("start_beam", self, "_on_start_beam")
	$Wizard.connect("start_beam_bomb", self, "_on_start_beam_bomb")
	$FireTimer.connect("timeout", self, "_on_spread_fire")

func _input(event):
	if event.is_action_pressed("tutorial"):
		get_tree().change_scene("res://game/tutorial/tutorial_move.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	print($Wizard.position)

func bomb():
	return $Wizard/WeaponsGUI/Tb1.active

func lazer():
	return $Wizard/WeaponsGUI/Tb2.active

func fire():
	return $Wizard/WeaponsGUI/Tb3.active

func cut():
	return $Wizard/WeaponsGUI/Tb4.active

func _on_Deathplane_body_entered(body):
	print(body, "Has gone to the death plane")
	if body.get_collision_layer_bit(1):
		Sfx.stream = death_sfx
		Sfx.play()
		get_tree().reload_current_scene()
	else:
		body.queue_free()
