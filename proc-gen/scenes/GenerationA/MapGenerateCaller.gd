extends Node2D

const TILE_HEIGHT = 16
const TILE_WIDTH = 16

const FULL_GRASS = Vector2(0, 0)
const GRASS_SW = Vector2(3, 0)
const GRASS_SE = Vector2(4, 0)
const GRASS_NE = Vector2(5, 0)
const GRASS_NW = Vector2(6, 0)
const FULL_SOIL = Vector2(0, 1)
const SOIL_SW = Vector2(3, 1)
const SOIL_SE = Vector2(4, 1)
const SOIL_NE = Vector2(5, 1)
const SOIL_NW = Vector2(6, 1)
const CELL_TYPES = [FULL_GRASS, GRASS_SW, GRASS_SE, GRASS_NE, GRASS_NW, 
					FULL_SOIL, SOIL_SW, SOIL_SE, SOIL_NE, SOIL_NW]
const SENTINEL_VEC = Vector2.INF
const CLEAR_VEC = Vector2(-1, -1)

const UP_LEFT = Vector2(-1, -1)
const UP = Vector2(0, -1)
const UP_RIGHT = Vector2(1, -1)
const RIGHT = Vector2(1, 0)
const DOWN_RIGHT = Vector2(1, 1)
const DOWN = Vector2(0, 1)
const DOWN_LEFT = Vector2(-1, 1)
const LEFT = Vector2(-1, 0)
const CIRCLE_DIRS = [UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT]
const DIAGONALS = [UP_LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT]
const CARDINALS = [UP, RIGHT, DOWN, LEFT]
var CORNER2TILE: Dictionary = {UP_LEFT: {FULL_GRASS: GRASS_SE, FULL_SOIL: SOIL_SE, 
						GRASS_SE: SENTINEL_VEC, SOIL_SE: SENTINEL_VEC},
					UP_RIGHT: {FULL_GRASS: GRASS_SW, FULL_SOIL: SOIL_SW, 
						GRASS_SW: SENTINEL_VEC, SOIL_SW: SENTINEL_VEC},
					DOWN_RIGHT: {FULL_GRASS: GRASS_NW, FULL_SOIL: SOIL_NW, 
						GRASS_NW: SENTINEL_VEC, SOIL_NW: SENTINEL_VEC},
					DOWN_LEFT: {FULL_GRASS: GRASS_NE, FULL_SOIL: SOIL_NE, 
						GRASS_NE: SENTINEL_VEC, SOIL_NE: SENTINEL_VEC}}


const BEAM_START = {UP_LEFT: Vector2(0, 2), UP: Vector2(0, 0), UP_RIGHT: Vector2(1, 2),
					RIGHT: Vector2(1, 0), DOWN_RIGHT: Vector2(2, 2), DOWN: Vector2(2, 0),
					DOWN_LEFT: Vector2(3, 2), LEFT: Vector2(3, 0)}
const BEAM_CONTINUE = {UP_LEFT: Vector2(2, 1), UP: Vector2(0, 1), UP_RIGHT: Vector2(3, 1),
					RIGHT: Vector2(1, 1), DOWN_RIGHT: Vector2(2, 1), DOWN: Vector2(0, 1),
					DOWN_LEFT: Vector2(3, 1), LEFT: Vector2(1, 1)}
const BEAM_CORNER = {UP_LEFT: Vector2(0, 3), UP_RIGHT: Vector2(1, 3), 
					DOWN_RIGHT: Vector2(2, 3), DOWN_LEFT: Vector2(3, 3)}
const BEAM_SPEED = 0.5
const BEAM_TILE = 5

const destr_particles = preload("res://destructors/CPUParticles2D.tscn")

const bomb_sfx = preload("res://sounds/bomb.wav")
const death_sfx = preload("res://sounds/death.wav")
const fire_sfx = preload("res://sounds/fire.wav")
const lazer_sfx = preload("res://sounds/lazer.wav")
const bb_sfx = preload("res://sounds/beam_bomb.wav")

# TODO: be able to use PoolVector2Array
var fire_cells: Array = []
var time_ticker = 0
var safe_spawn_startduration = 30
var wizard_Spawned = false

func destructor(dir, loc=null):
	if loc == null:
		loc = $LevelGenerateA.world_to_map(position)
	if $LevelGenerateA.get_cellv(loc + dir) != -1:
		var parts = destr_particles.instance()
		parts.position = $LevelGenerateA.map_to_world(loc + dir)
		add_child(parts)
	$LevelGenerateA.set_cellv(loc + dir, -1)

func carver(dir, loc=null):
	if loc == null:
		loc = $LevelGenerateA.world_to_map(position)
	var dest = loc + dir
	if $LevelGenerateA.get_cellv(dest) == -1:
		return
	var orig_type = $LevelGenerateA.get_cell_autotile_coord(dest.x, dest.y)
	print("carver dest", dest)
	print("carver type", orig_type)
	var new_type = Vector2.ZERO
	var clear_tile = false

	# up right or down left is ne/sw
	if dir == UP_RIGHT or dir == DOWN_LEFT:
		if orig_type == FULL_GRASS:
			new_type = GRASS_SE
		elif orig_type == FULL_SOIL:
			new_type = SOIL_SE
		elif orig_type == GRASS_NE or orig_type == GRASS_SW or orig_type == SOIL_NE or orig_type == SOIL_SW:
			clear_tile = true

	# up left or down right is nw/se
	if dir == UP_LEFT or dir == DOWN_RIGHT:
		if orig_type == FULL_GRASS:
			new_type = GRASS_SW
		elif orig_type == FULL_SOIL:
			new_type = SOIL_SW
		elif orig_type == GRASS_SE or orig_type == GRASS_NW or orig_type == SOIL_SE or orig_type == SOIL_NW:
			clear_tile = true

	if clear_tile:
		if $LevelGenerateA.get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = $LevelGenerateA.map_to_world(dest)
			add_child(parts)
		$LevelGenerateA.set_cellv(dest, -1)
	elif new_type != Vector2.ZERO:
		$LevelGenerateA.set_cell(dest.x, dest.y, 0, false, false, false, new_type)

func diag_cut(corner, dest):
	# corner is the corner to delete
	if $LevelGenerateA.get_cellv(dest) == -1:
		# don't cut an empty tile
		return
	var orig_type = $LevelGenerateA.get_cell_autotile_coord(dest.x, dest.y)
	var entry = CORNER2TILE[corner]
	if orig_type in entry:
		if entry[orig_type] == SENTINEL_VEC:
			pass
		else:
			var parts = destr_particles.instance()
			parts.position = $LevelGenerateA.map_to_world(dest)
			add_child(parts)
			$LevelGenerateA.set_cell(dest.x, dest.y, 0, false, false, false, entry[orig_type])
	else:
		if $LevelGenerateA.get_cellv(dest) != -1:
			var parts = destr_particles.instance()
			parts.position = $LevelGenerateA.map_to_world(dest)
			add_child(parts)
		$LevelGenerateA.set_cellv(dest, -1)
		print("cleared tile is", $LevelGenerateA.get_cellv(dest))

# Called when the node enters the scene tree for the first time.
func _ready():
	$LevelGenerateA.setdimension(80, 60)
	$LevelGenerateA.getproceduralmap()
	var tSet = $LevelGenerateA.get_tileset()
	var strGateCoords = $LevelGenerateA.getgateportalcoordinates()
	var strKeysCoords = $LevelGenerateA.getkeyplacementcoordinates()
	placeGateFromStringCoordinates(strGateCoords)
	
	#print(tSet)
	#var tileItems = tSet.get_tiles_ids()
	#print(tileItems)

func ConvertStringToCoordinate(coordStr : String) -> Vector2:
	var res : Vector2 = Vector2()
	coordStr.replace('(', '')
	coordStr.replace(')', '')
	var crdparts = coordStr.split(",", false, 2)
	if crdparts.size() >= 2:
		res.x = int(crdparts[0])
		res.y = int(crdparts[1])
	return res

func validGateTilePosition(coordinate : Vector2) -> bool:
	var res = true
	if coordinate.x <= 8 or coordinate.y <= 10:
		return false
	for y in range(coordinate.y - 3, coordinate.y):
		for x in range(coordinate.x - 1, coordinate.x  + 2):
			if $LevelGenerateA.get_cell(x, y) > 0:
				res = false
				break
	return res

func placeKeysFromStringCoordinatesMap(coordsMap : Dictionary) -> void:
	if coordsMap.size() >= 2:
		for regionKey in coordsMap.keys():
			print('placeKeysFromStringCoordinatesMap, regionKey=%s\n' % [regionKey])
			var coordsInBlock = coordsMap[regionKey]

func placeGateFromStringCoordinates(coords : String) -> void:
	if coords.length() >= 8:
		var fields = coords.split(";", false, 30)
		#print('placeGateFromStringCoordinates, coords=%s, #fields=%d\n' % [coords, fields.size()])
		for coordStr in fields:
			var coordTile = ConvertStringToCoordinate(coordStr)
			if validGateTilePosition(coordTile):
				print('placeGateFromStringCoordinates, valid gate pos = %s' % [coordTile])
				$LevelGenerateA/Gate.position = Vector2( (coordTile.x + 1) * TILE_WIDTH, (coordTile.y - 2) * TILE_HEIGHT)
				print('placeGateFromStringCoordinates, wizard=%s' % [$LevelGenerateA/Wizard])
				Globals.wizard_spawn = Vector2((coordTile.x + 1) * TILE_WIDTH, (coordTile.y - 2) * TILE_HEIGHT)
				#globals.setWizardSpawn(Vector2(coordTile.x * TILE_WIDTH, (coordTile.y - 2) * TILE_HEIGHT))
				break

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func on_start_bomb(loc):
	Sfx.stream = bomb_sfx
	Sfx.play()
	$LevelGenerateA/VFXTileMap.set_cellv(loc, 1)
	var new_loc
	var i
	var bomb_time = 0.2
	for f in [2, 3]:
		i = 0
		yield(get_tree().create_timer(bomb_time), "timeout")
		for col in range(3):
			for row in range(3):
				if row == 1 and col == 1:
					new_loc = loc
				else:
					new_loc = loc + CIRCLE_DIRS[i]
					i += 1
				$LevelGenerateA/VFXTileMap.set_cell(new_loc.x, new_loc.y, f, false, false, false, Vector2(col, row))
	yield(get_tree().create_timer(bomb_time), "timeout")
	for dir in CIRCLE_DIRS:
		destructor(dir, loc)
	# clean up vfx
	i = 0
	for col in range(3):
		for row in range(3):
			if row == 1 and col == 1:
				new_loc = loc
			else:
				new_loc = loc + CIRCLE_DIRS[i]
				i += 1
			$LevelGenerateA/VFXTileMap.set_cellv(new_loc, -1)

func on_start_beam(dir, loc):
	Sfx.stream = lazer_sfx
	Sfx.play()
	var dest = loc + dir
	var dest_cell = $LevelGenerateA.get_cellv(dest)
	var start = true
	var cut_y = Vector2(0, dir.y)
	var cut_x = Vector2(dir.x, 0)
	var corner_x
	var corner_y
	var corner_x_vfx
	var corner_y_vfx
	if dir in DIAGONALS:
		corner_x = Vector2(dir.x*-1, dir.y)
		corner_y = Vector2(dir.x, dir.y*-1)
		corner_x_vfx = BEAM_CORNER[corner_x]
		corner_y_vfx = BEAM_CORNER[corner_y]
	var vfx_tiles = []
	var tile
	var x_dest
	var y_dest
	while dest_cell != -1:
		if start:
			tile = BEAM_START[dir]
			start = false
		else:
			tile = BEAM_CONTINUE[dir]
		print("tile", tile)
		$LevelGenerateA/VFXTileMap.set_cell(dest.x, dest.y, BEAM_TILE, false, false, false, tile)
		vfx_tiles.append(dest)
		yield(get_tree().create_timer(BEAM_SPEED), "timeout")
		destructor(dir, loc)
		if dir in DIAGONALS:
			x_dest = dest + cut_x
			y_dest = dest + cut_y
			$LevelGenerateA/VFXTileMap.set_cell(x_dest.x, x_dest.y, BEAM_TILE, false, false, false, corner_x_vfx)
			vfx_tiles.append(x_dest)
			$LevelGenerateA/VFXTileMap.set_cell(y_dest.x, y_dest.y, BEAM_TILE, false, false, false, corner_y_vfx)
			vfx_tiles.append(y_dest)
			diag_cut(corner_x, x_dest)
			diag_cut(corner_y, y_dest)
		loc = dest
		dest = loc + dir
		dest_cell = $LevelGenerateA.get_cellv(dest)
	if dir in DIAGONALS:  # remove the last two "corner" VFX tiles
		for i in range(2):
			$LevelGenerateA/VFXTileMap.set_cellv(vfx_tiles.pop_back(), -1)
	yield(get_tree().create_timer(BEAM_SPEED), "timeout")
	for tile in vfx_tiles:
		$LevelGenerateA/VFXTileMap.set_cellv(tile, -1)
	return loc

func on_start_fire(dest):
	Sfx.stream = fire_sfx
	Sfx.play()
	fire_cells.append(dest)
	
	$LevelGenerateA/VFXTileMap.set_cellv(dest, 4)
	if $LevelGenerateA/FireTimer.is_stopped():
		$LevelGenerateA/FireTimer.start()

func on_start_cut(dir, loc):
#	if dir in DIAGONALS:
#		carver(dir, loc)
#	else:
	destructor(dir, loc)

func _on_Timer_timeout():
	time_ticker += 1
	if time_ticker >= safe_spawn_startduration and time_ticker < safe_spawn_startduration + 10:
		if !wizard_Spawned:
			wizard_Spawned = true
			$LevelGenerateA/Wizard.position = Globals.wizard_spawn
	var myActiveTool = Globals.getActiveTool()
	if myActiveTool.length() >= 2:
		var values = Globals.getToolEventFields(myActiveTool, true)
		#dir, loc, event_time
		var dir = values[0]
		var loc = values[1]
		match myActiveTool:
			"start_bomb":
				on_start_bomb(loc)
			"start_beam":
				on_start_beam(dir, loc)
			"start_fire":
				on_start_fire(loc)
			"start_cut":
				on_start_cut(dir, loc)
