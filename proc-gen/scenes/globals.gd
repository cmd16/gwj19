extends Node

var timestamp_class = preload("res://scenes/Timestamp.gd")

export var music_volume : int = 50
export var sound_volume : int = 75
export var wizard_spawn : Vector2 = Vector2(52, 40)
var activeBomb : bool = false
var activeLazer : bool = false
var activeCut : bool = false
var activeFire : bool = false
var activeBeam : bool = false
var tool_events : Dictionary = { }
var time_tool_check = timestamp_class.timestamp.new()

func activate_tool(tool_name, dir, loc, event_time):
	if tool_events.keys().size() >= 3:
		tool_events.clear()
	if tool_events.has(tool_name):
		tool_events.erase(tool_name)
	tool_events[tool_name] = [dir, loc, event_time]

func getToolEventFields(tool_name, clear):
	if tool_events.has(tool_name):
		var res = tool_events[tool_name]
		if clear:
			tool_events.erase(tool_name)
		return res
	return null

func getActiveTool():
	time_tool_check.reset_time()
	var loopAgain = true
	while loopAgain:
		loopAgain = false
		for key in tool_events.keys():
			var event_value = tool_events[key]
			var event_time = event_value[2]
			var ticksElapsed = time_tool_check.subtract_ticks(event_time)
			if ticksElapsed >= 800:
				tool_events.erase(key)
				loopAgain = true
			print('getActiveTool, tool=%s\n' % [key])
			print('getActiveTool, time_tool_check=%s, time-diff=%d\n' % [time_tool_check.quickprint(), ticksElapsed])
			if ticksElapsed <= 500:
				return key
	return ""

func switchOffAllTools():
	activeBeam = false
	activeBomb = false
	#activeLazer = false
	activeCut = false
	activeFire = false
	print('switchOffAllTools\n')

func setBeam(active):
	activeBeam = active
	print('setBeam, active=%s\n' % [active])

func setBomb(active):
	activeBomb = active
	print('setBomb, active=%s\n' % [active])

func setCut(active):
	activeCut = active
	print('setCut, active=%s\n' % [active])

func setFire(active):
	activeFire = active
	print('setFire, active=%s\n' % [active])

func isBeam():
	return activeBeam

func isBomb():
	return activeBomb

func isCut():
	return activeCut
	
func isFire():
	return activeFire
