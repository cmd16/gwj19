/* 
 * File:   CavernMapGenerator.h
 * Author: raoul
 *
 * Created on 21 March 2020, 20:32
 */

#ifndef CAVERNMAPGENERATOR_H
#define CAVERNMAPGENERATOR_H

#include <string>
#include <memory>
#include <utility>
#include <unordered_map>
//#include "TileKey.h"
#include "RandomUtility.h"

class CavernMapGenerator {
public:
    CavernMapGenerator();
    CavernMapGenerator(const CavernMapGenerator& orig);
    virtual ~CavernMapGenerator();

    void GenerateMap(bool invert = false);
    void RandomSelectFillRows(int minRows, int maxRows,int minHeight, int maxHeight, int value);
    void RandomSelectFillColumns(int minColumns, int maxColumns, int minWidth, int maxWidth, int value);
    void FillRow(int rowIndex, int value);
    void FillColumn(int colIndex, int value);
    void Overlay(CavernMapGenerator& mapGenr, int operation = 0);
    void SliceBlock(float xProp, float yProp, int blkWidth, int blkHeight, int value);
    bool IsWall(int column, int row);
    void RemoveClustersBelow(int belowCount);
    int GetContiguousFilled(int x, int y);
    int PlaceWallLogic(int x, int y);
    int GetAdjacentWalls(int x, int y, int scopeX, int scopeY);
    bool IsOutOfBounds(int x, int y);
    void RandomFillMap();
    std::string MapToString();
    void BlankMap();
    void WriteMapFile(std::string workPath, std::string specFile = "");
    std::vector<int> GetTileVector();

    void SetMapSize(std::pair<int, int> mapSize);
    std::pair<int, int> GetMapSize() const;
    void SetWallsRatio(float wallsRatio);
    float GetWallsRatio() const;
    void SetMapName(std::string mapName);
    std::string GetMapName() const;
private:
    std::string mapName;
    float wallsRatio = 0.6;
    bool displayHeader = true;
    std::pair<int, int> mapSize;
    std::vector<int> mapVectorTiles;
    //std::unique_ptr<std::unordered_map <TileKey, int, TileKeyHashFunc>> mapTiles;
    RandomUtility randGen;

    int RandomPercent(float ratio);
    int CombineValue(int value1, int value2, int operation = 0);    
 
};

#endif /* CAVERNMAPGENERATOR_H */

