/* 
 * File:   argumentrecord.h
 * Author: Raoul
 *
 * Created on 01 May 2012, 11:24 PM
 */

#ifndef ARGUMENTRECORD_H
#define	ARGUMENTRECORD_H

class argumentrecord {
public:
    argumentrecord(int _argc, char** _argv);
    argumentrecord(const argumentrecord& orig);
    virtual ~argumentrecord();
    
    int argc;
    char **argv;
private:
    
};

#endif	/* ARGUMENTRECORD_H */

