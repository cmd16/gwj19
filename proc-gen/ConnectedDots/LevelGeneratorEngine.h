/* 
 * File:   LevelGeneratorEngine.h
 * Author: raoul
 *
 * Created on 25 March 2020, 15:22
 */

#ifndef LEVELGENERATORENGINE_H
#define LEVELGENERATORENGINE_H

#include <string>
#include <memory>
#include "CavernMapGenerator.h"

class LevelGeneratorEngine {
public:
    LevelGeneratorEngine(int width, int height);
    LevelGeneratorEngine(const LevelGeneratorEngine& orig);
    virtual ~LevelGeneratorEngine();
    
    void setFillDistributionRange(float min, float max);
    void setProductionFile(std::string section, std::string fileName);
private:
    int width, height;
    std::unique_ptr<CavernMapGenerator> cavernMapGen;
};

#endif /* LEVELGENERATORENGINE_H */

