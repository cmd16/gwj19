#include <algorithm>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <utility>
#include <string>
#include <map>
#include "argparse.h"
#include "extrahelper.h"
#include "imghelpers.h"
#include "directions.h"
#include "RandomUtility.h"
#include "CavernMapGenerator.h"
#include "ConnectedComponentsFinder.h"

using namespace argparse;

bool connectFindFromFile = false;
std::pair<int, int> mapTileSpan = std::make_pair(80, 60);
std::pair<int, int> cavityWidthRange = std::make_pair(3, 6);
std::pair<int, int> cavityHeightRange = std::make_pair(2, 4);
std::map<std::string, std::string> argumentFieldValues;
float fillRatio = 0.45;
unsigned int mapsToGenerate = 16;
std::string filePrefix;
std::string filePostfixFormat;

std::vector<uint> ConvertIntVector(std::vector<int> input) {
    std::vector<uint> inputVertices;
    for (int val : input) {
        inputVertices.push_back((uint) val);
    }
    return inputVertices;
}

bool RegionWithinBounds(uint regionNum, int x, int y, int width, int height, int blocks, std::vector<uint> regionMap) {
    bool res = false;
    uint mapwidth = mapTileSpan.first;
    uint mapheight = mapTileSpan.second;
    std::vector<uint>::iterator iter = regionMap.begin();
    while ((iter = std::find_if(iter, regionMap.end(), [regionNum](uint i) {
            return (i == regionNum);
        })) != regionMap.end()) {
    int idx = std::distance(regionMap.begin(), iter);
    int ccol = idx % mapwidth;
    int crow = idx / mapwidth;
    if ((ccol >= x) && (ccol <= x + width)) {
        if ((crow >= y) && (crow < y + height)) {
            res = true;
            break;
        }
    }
    iter++;
}
    return res;
}

std::vector<uint> GetRegionTileCoordinates(uint regionNum, std::vector<uint> regionMap) {
    uint mapwidth = mapTileSpan.first;
    uint mapheight = mapTileSpan.second;
    std::vector<uint> res;
    std::vector<uint>::iterator iter = regionMap.begin();
    while ((iter = std::find_if(iter, regionMap.end(), [regionNum](uint i) {
            return (i == regionNum);
        })) != regionMap.end()) {
    int idx = std::distance(regionMap.begin(), iter);
    res.push_back(idx);
    iter++;
}
    return res;
}

std::string WriteCoordinatesString(std::vector<uint> regionTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::stringstream outgen;
    for (uint tile : regionTileCoords) {
        int ccol = tile % mapwidth;
        int crow = tile / mapwidth;
        outgen << "(" << ccol << "," << crow << ")";
        outgen << ";";
    }
    return outgen.str();
}

void WriteCoordinates(std::string coordFile, std::string regionTag, std::vector<uint> regionTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::ofstream outfile((const char*) coordFile.c_str(), std::ofstream::out);
    for (uint tile : regionTileCoords) {
        int ccol = tile % mapwidth;
        int crow = tile / mapwidth;
        outfile << "(" << ccol << "," << crow << ")";
        outfile << std::endl;
    }
    outfile.close();
}

std::string WriteCoordinatesMapString(std::unordered_map<uint, std::vector<uint>> mapTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::stringstream outgen;
    for (std::pair<uint, std::vector < uint >> element : mapTileCoords) {
        uint region = element.first;
        std::vector<uint> coordinatesList = element.second;
        outgen << region << "=[";
        std::string strTileCoordinates = WriteCoordinatesString(coordinatesList);
        outgen << strTileCoordinates;
        outgen << "]$";
    }
    return outgen.str();
}

std::shared_ptr<std::vector<std::pair<int, int>>> FindTopLeftMatchingBlockPatterns(int width, int height, int blockWidth, int blockHeight, std::vector<uint> mapCoords, std::vector<uint> blockPattern) {
    std::shared_ptr<std::vector<std::pair<int, int>>> res = std::make_shared<std::vector<std::pair<int, int>>>();
    if ((width >= 2) && (height >= 2)) {
        if (blockPattern.size() <= (width * height)) {
            for (int ix = 0; ix < height; ix++) {
                std::vector<uint> myRowVector(width);
                std::copy(mapCoords.begin() + (width * ix), mapCoords.begin() + (width * ix) + width - 1, myRowVector.begin());
                //See if the current match row of blockPattern is found in myRowVector
            }
        }
    }
    return res;
}

void CreateLevelMaps(std::string descriptor, unsigned int createCount) {
    RandomUtility randGmaze;
    randGmaze.setRandomSeed(time(0));
    uint xLen = mapTileSpan.first; // 80;
    uint yLen = mapTileSpan.second; //60;
    uint connectivity = 8;
    for (int mapcidx = 0; mapcidx < createCount; mapcidx++) {
        std::pair<int, int> mapTileSpan = std::make_pair(xLen, yLen);
        float range1 = randGmaze.getDoubleInRange(0.32, 0.41);
        float range2 = randGmaze.getDoubleInRange(0.39, 0.46);
        int blockSliceWidth = randGmaze.getIntInRange(21, 27);
        int blockSliceHeight = randGmaze.getIntInRange(4, 6);
        int bottomBlockSliceWidth = randGmaze.getIntInRange(18, 24);
        int bottomBlockSliceHeight = randGmaze.getIntInRange(6, 9);
        std::unique_ptr<CavernMapGenerator> cavernBlobMap1 = std::make_unique<CavernMapGenerator>();
        std::unique_ptr<CavernMapGenerator> cavernBlobMap2 = std::make_unique<CavernMapGenerator>();
        char debugText[4096];
        memset(debugText, 0, 4096);
        snprintf(debugText, 4095, "[1]MazeGenerator::generateMaze(), map index=%d, range1=%f, range2=%f \n", mapcidx, range1, range2);
        std::stringstream fmtFileGenerate;

        if ((cavernBlobMap1 != nullptr) && (cavernBlobMap1 != nullptr)) {
            cavernBlobMap1->SetMapName("platform1");
            cavernBlobMap2->SetMapName("platform2");
            cavernBlobMap1->SetMapSize(mapTileSpan);
            cavernBlobMap2->SetMapSize(mapTileSpan);
            cavernBlobMap1->BlankMap();
            cavernBlobMap2->BlankMap();
            cavernBlobMap1->SetWallsRatio(range1);
            cavernBlobMap2->SetWallsRatio(range2);
            cavernBlobMap1->RandomFillMap();
            cavernBlobMap2->RandomFillMap();
            std::string mapData1 = cavernBlobMap1->MapToString();
            std::string mapData2 = cavernBlobMap2->MapToString();
            std::cout << mapData1 << std::endl << mapData2;
            cavernBlobMap1->GenerateMap(true);
            cavernBlobMap2->GenerateMap(true);
            cavernBlobMap1->RandomSelectFillRows(2, 3, 3, 4, 0);
            cavernBlobMap2->RandomSelectFillRows(3, 4, 2, 3, 0);
            cavernBlobMap1->RandomSelectFillColumns(2, 4, 2, 4, 0);
            cavernBlobMap2->RandomSelectFillColumns(1, 3, 3, 4, 0);
            cavernBlobMap2->SliceBlock(0.32, 0.63, bottomBlockSliceWidth, bottomBlockSliceHeight, 0);
            cavernBlobMap1->Overlay(*cavernBlobMap2, 0);
            cavernBlobMap1->SliceBlock(0.21, 0.45, blockSliceWidth, blockSliceHeight, 0);
            auto vecTiles = cavernBlobMap1->GetTileVector();
            std::vector<uint> vecGeneratedMap = ConvertIntVector(vecTiles);
            fmtFileGenerate << descriptor << "_in#" << mapcidx << ".map";
            std::string outFile = fmtFileGenerate.str();
            fmtFileGenerate.clear();
            WriteImgToFile(outFile, vecGeneratedMap, xLen, yLen);
            ConnectedComponentsFinder componentFinder(vecTiles, xLen, yLen);
            componentFinder.FindConnectedComponentsInput();
            auto resOut = componentFinder.GetOutVertices();
            int eraseBlockThreshold = 3;

            //Get the set of unique labels in the resOut map that is the output of the connected component finder
            std::string workPathGen = ExtraHelper::getWorkingPath();
            std::vector<uint> filterFields = resOut;
            std::vector<uint> limitEraseRegions;
            std::unordered_map<uint, int> regionCountMap;
            std::sort(filterFields.begin(), filterFields.end());
            filterFields.erase(std::unique(filterFields.begin(), filterFields.end()), filterFields.end());
            for (uint regionNum : filterFields) {
                //For each label determine the find count of that label in the vector map "resOut"
                std::vector<uint> filterRegionCount(resOut.size());
                auto it = std::copy_if(resOut.begin(), resOut.end(), filterRegionCount.begin(), [regionNum](uint i) {
                    return (i == regionNum);
                });
                filterRegionCount.resize(std::distance(filterRegionCount.begin(), it));
                regionCountMap[regionNum] = filterRegionCount.size();
                std::cout << "Region[" << regionNum << "] = " << filterRegionCount.size() << std::endl;
                if (filterRegionCount.size() <= eraseBlockThreshold) {
                    limitEraseRegions.push_back(regionNum);
                }
            }
            for (uint removeregion : limitEraseRegions) {
                std::vector<uint>::iterator iter = resOut.begin();
                while ((iter = std::find_if(iter, resOut.end(), [removeregion](uint i) {
                        return (i == removeregion);
                    })) != resOut.end()) {
                int idx = std::distance(resOut.begin(), iter);
                vecGeneratedMap[idx] = 0;
                std::cout << idx;
                iter++;
            }
            }

            //Now find the regions again on the cleaned up "vecGeneratedMap"
            ConnectedComponentsFinder componentFinder2(vecGeneratedMap, mapTileSpan.first, mapTileSpan.second);
            componentFinder2.FindConnectedComponentsInput();
            auto resOut2 = componentFinder2.GetOutVertices();
            std::string descriptor = ExtraHelper::printShortTimestamp();
            fmtFileGenerate << descriptor << "_out#" << mapcidx << ".map";
            outFile = fmtFileGenerate.str();
            fmtFileGenerate.clear();
            RelabelImg(resOut2, xLen, yLen);
            WriteImgToFile(outFile, resOut2, xLen, yLen);
            filterFields = resOut2;
            //std::vector<uint> blockRegions;
            regionCountMap.clear();
            std::sort(filterFields.begin(), filterFields.end());
            filterFields.erase(std::unique(filterFields.begin(), filterFields.end()), filterFields.end());
            for (uint regionNum : filterFields) {
                //For each label determine the find count of that label in the vector map "resOut"
                std::vector<uint> filterRegionCount(resOut.size());
                auto it = std::copy_if(resOut.begin(), resOut.end(), filterRegionCount.begin(), [regionNum](uint i) {
                    return (i == regionNum);
                });
                filterRegionCount.resize(std::distance(filterRegionCount.begin(), it));
                regionCountMap[regionNum] = filterRegionCount.size();
            }
            uint regionPortal = 0;
            for (uint regionNum : filterFields) {
                if (regionNum <= 1)
                    continue;
                if (RegionWithinBounds(regionNum, 12, 23, (xLen * 0.55), (yLen * 0.35), 14, resOut2)) {
                    //Found a suitable centre "origin" block
                    //Find the top surface coordinates of this block
                    regionPortal = regionNum;
                    std::vector<uint> regionTileCoords = GetRegionTileCoordinates(regionNum, resOut2);
                    char regionTag[100];
                    memset(regionTag, 0, 100);
                    //outFile = descriptor + "_gatezone.crd";
                    fmtFileGenerate << "gatezone#" << mapcidx << ".map";
                    std::string outFile = fmtFileGenerate.str();
                    fmtFileGenerate.clear();
                    WriteCoordinates(outFile, regionTag, regionTileCoords);
                    break;
                }
            }

            //Find the key area blocks....
            int keyCounter = 0;
            std::unordered_map<uint, std::vector < uint>> regionKeyCoordsMap;
            for (uint regionNum : filterFields) {
                if ((regionNum <= 1) || (regionNum == regionPortal))
                    continue;
                if (RegionWithinBounds(regionNum, 11, 12, (xLen * 0.72), (yLen * 0.62), 9, resOut2)) {
                    //Find the top surface coordinates of this block
                    std::stringstream debugRegionStream;
                    debugRegionStream << "Region #" << regionNum << " within bounds (";
                    debugRegionStream << 11 << "," << 12 << ",width=" << int(xLen * 0.72);
                    debugRegionStream << ",height=" << int(yLen * 0.62) << ") ";
                    debugRegionStream << " block count=" << regionCountMap[regionNum] << std::endl;
                    std::cout << "Result map = " << debugRegionStream.str() << std::endl;
                    std::vector<uint> keyTileCoords = GetRegionTileCoordinates(regionNum, resOut2);
                    regionKeyCoordsMap[regionNum] = keyTileCoords;
                    keyCounter++;
                }
            }
            std::string strKeysCoordinates = WriteCoordinatesMapString(regionKeyCoordsMap);

            fmtFileGenerate << "gencavern_key_regions#" << mapcidx << ".map";
            outFile = fmtFileGenerate.str();
            fmtFileGenerate.clear();
            std::string fullPathData = ExtraHelper::combineFilePath(workPathGen, outFile);
            std::ofstream outfileMap((const char*) fullPathData.c_str(), std::ofstream::out);
            outfileMap << strKeysCoordinates << std::endl;
            outfileMap.close();

            std::string debugVectorOut = ExtraHelper::printDebugVector<uint>("", 40, resOut);
            std::cout << "Result map = " << debugVectorOut << std::endl;
            fmtFileGenerate << "result-regions#" << mapcidx << ".map";
            outFile = fmtFileGenerate.str();
            fmtFileGenerate.clear();
            RelabelImg(resOut, xLen, yLen);
            WriteImgToFile(outFile, resOut, xLen, yLen);
        }
    }
}

void processArguments(int argc, char **argv, bool &generateTask, std::vector<std::string>& res) {
    generateTask = true;
    ArgumentParser parser("Connected dots application arguments");
    parser.add_argument()
            .names({"-v", "--version"})
    .description("version")
            .required(false);
    parser.add_argument()
            .names({"-L", "--logpath"})
    .description("logpath")
            .required(false);
    parser.add_argument()
            .names({"-W", "--workingpath"})
    .description("workingpath")
            .required(false);
    parser.add_argument()
            .names({"-f", "--filetag"})
    .description("filetag")
            .required(false);
    parser.add_argument()
            .names({"-p", "--properties"})
    .description("properties")
            .required(false);
    parser.enable_help();
    auto err = parser.parse(argc, (const char**) argv);
    if (err) {
        std::cout << err << std::endl;
        return;
    }
    if (parser.exists("help")) {
        parser.print_help();
        generateTask = false;
        return;
    }
    if (parser.exists("v")) {
        res.push_back("version");
        std::cout << "Version: 0.5\t\tDate: 04/04/2020\n";
        generateTask = false;
        return;
    }
    if (parser.exists("L")) {
        res.push_back("logpath");
        argumentFieldValues["logpath"] = parser.get<std::string>("L");
    }
    if (parser.exists("W")) {
        res.push_back("workingpath");
        argumentFieldValues["workingpath"] = parser.get<std::string>("W");
    }
    if (parser.exists("f")) {
        res.push_back("filetag");
        argumentFieldValues["filetag"] = parser.get<std::string>("f");
    }
    if (parser.exists("p")) {
        res.push_back("properties");
        argumentFieldValues["properties"] = parser.get<std::string>("p");
    }
}

int main(int argc, char **argv) {
    std::string infile = "";
    std::string outfile = "output";
    std::string fileTagPrefix = "procgenmap";
    uint connectivity = 4;
    bool generateTask = false;
    std::vector<std::string> appCommandArguments;
    //dimensions={}
    //mapoutprefix={}
    //outputpath={}
    //genproperties{connectivity, ???}
    processArguments(argc, argv, generateTask, appCommandArguments);
    /*
    if (argc > 3) {
        connectivity = atoi(argv[3]);
    }
     */
    if (argumentFieldValues.size() > 0) {
        // Create an iterator of map
        std::map<std::string, std::string>::iterator it;

        it = argumentFieldValues.find("filetag");
        if (it != argumentFieldValues.end()) {
            fileTagPrefix = argumentFieldValues["filetag"];
        }
    }
    if (generateTask) {
        uint xLen, yLen;
        std::cout << "path = " << ExtraHelper::getWorkingPath() << std::endl;
        if (connectFindFromFile) {
            std::vector<uint> vec = ReadImgFromFile(infile, xLen, yLen);
            std::vector<uint> res(vec.size());
            //FindConnectedComponents(vec, res, xLen, yLen, connectivity);
            RelabelImg(res, xLen, yLen);
            WriteImgToFile(outfile, res, xLen, yLen);
        } else {
            CreateLevelMaps(fileTagPrefix + "#" + ExtraHelper::printShortTimestamp(), mapsToGenerate);
        }
    }
    return 0;
}

