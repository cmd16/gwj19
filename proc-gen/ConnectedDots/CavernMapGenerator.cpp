/* 
 * File:   CavernMapGenerator.cpp
 * Author: raoul
 * 
 * Created on 21 March 2020, 20:32
 */

#include <fstream>
#include <ctime>
#include "extrahelper.h"
#include "CavernMapGenerator.h"

CavernMapGenerator::CavernMapGenerator() {
    //mapTiles = std::make_unique<std::unordered_map <TileKey, int, TileKeyHashFunc> >();
    mapVectorTiles.reserve(mapSize.first * mapSize.second);
    for (int x = 0; x < mapSize.first; x++) {
        for (int y = 0; y < mapSize.second; y++) {
            mapVectorTiles.push_back(0);
        }
    }
    randGen.setRandomSeed(time(0));
}

CavernMapGenerator::CavernMapGenerator(const CavernMapGenerator& orig) {
    mapVectorTiles.reserve(mapSize.first * mapSize.second);
    for (int x = 0; x < mapSize.first; x++) {
        for (int y = 0; y < mapSize.second; y++) {
            mapVectorTiles.push_back(0);
        }
    }
    randGen.setRandomSeed(time(0));
}

CavernMapGenerator::~CavernMapGenerator() {
}

void CavernMapGenerator::GenerateMap(bool invert) {
    for (int row = 0; row <= mapSize.second - 1; row++) {
        for (int column = 0; column <= mapSize.first - 1; column++) {
            //(*mapTiles)[TileKey("Row", std::make_pair(column, row))] = PlaceWallLogic(column, row);
            mapVectorTiles[column + (row * (mapSize.first))] = PlaceWallLogic(column, row);
        }
    }
    if (invert) {
        for (int row = 0; row <= mapSize.second - 1; row++) {
            for (int column = 0; column <= mapSize.first - 1; column++) {
                mapVectorTiles[column + (row * (mapSize.first))] = mapVectorTiles[column + (row * (mapSize.first))] == 1 ? 0 : 1;
            }
        }
    }
}

void CavernMapGenerator::RandomSelectFillRows(int minRows, int maxRows, int minHeight, int maxHeight, int value) {
    int selectedRowZones = randGen.getIntInRange(minRows, maxRows);
    int rowSpacing = (mapSize.second / selectedRowZones) - 1;
    int startRow = 0;
    int prevRowHeight = 0;
    for (int ir = 0; ir < selectedRowZones; ir++) {
        int currentZoneHeight = randGen.getIntInRange(minHeight, maxHeight);
        startRow += randGen.getIntInRange(2, rowSpacing) + prevRowHeight;
        prevRowHeight = currentZoneHeight;
        for (int rowzone = 0; rowzone < currentZoneHeight; rowzone++) {
            FillRow(rowzone + startRow, value);
        }
    }
}

void CavernMapGenerator::RandomSelectFillColumns(int minColumns, int maxColumns, int minWidth, int maxWidth, int value) {
    int selectedColumnZones = randGen.getIntInRange(minColumns, maxColumns);
    int colSpacing = (mapSize.first / selectedColumnZones) - 1;
    int startCol = 0;
    int prevColWidth = 0;
    for (int ir = 0; ir < selectedColumnZones; ir++) {
        int currentZoneWidth = randGen.getIntInRange(minWidth, maxWidth);
        startCol += randGen.getIntInRange(2, colSpacing) + prevColWidth;
        prevColWidth = currentZoneWidth;
        for (int colzone = 0; colzone < currentZoneWidth; colzone++) {
            FillColumn(colzone + startCol, value);
        }
    }
}

void CavernMapGenerator::FillRow(int rowIndex, int value) {
    if ((rowIndex >= 0) && (rowIndex < mapSize.second)) {
        for (int ix = 0; ix < mapSize.first; ix++)
            mapVectorTiles[ix + (rowIndex * (mapSize.first))] = value;
    }
}

void CavernMapGenerator::FillColumn(int colIndex, int value) {
    if ((colIndex >= 0) && (colIndex < mapSize.first)) {
        for (int ix = 0; ix < mapSize.second; ix++)
            mapVectorTiles[colIndex + (ix * (mapSize.first))] = value;
    }
}

void CavernMapGenerator::SliceBlock(float xProp, float yProp, int blkWidth, int blkHeight, int value) {
    int xpos = xProp * mapSize.first;
    int ypos = yProp * mapSize.second;
    int currentRun = 0;
    int startIndex = xpos + (ypos * mapSize.first);
    for (int ix = xpos + (ypos * mapSize.first); ix < (ypos + blkHeight) * mapSize.first; ix++) {
        if (currentRun < blkWidth) {
            mapVectorTiles[ix] = value;
            currentRun++;
        } else {
            if ( ((ix - startIndex) % mapSize.first) == mapSize.first - 1) {
                currentRun = 0;
            }
        }
    }
}

void CavernMapGenerator::Overlay(CavernMapGenerator& mapGenr, int operation) {
        std::vector<int> genrTiles = mapGenr.GetTileVector();
    for (int row = 0; row <= mapSize.second - 1; row++) {
        for (int column = 0; column <= mapSize.first - 1; column++) {
            mapVectorTiles[column + (row * (mapSize.first))] =
                CombineValue(mapVectorTiles[column + (row * (mapSize.first))], genrTiles[column + (row * (mapSize.first))], operation);
        }
    }
}

void CavernMapGenerator::RemoveClustersBelow(int belowCount) {
    int scopeX = 2;
    int scopeY = 2;
    if (belowCount > 8) {
        scopeX = 3;
        scopeY = 3;
    }
    int rowOffset = 0;
    int colOffset = 0;
    bool scanCompleted = false;
    while (!scanCompleted) {
        for (int row = rowOffset; row <= mapSize.second - 1; row += 2) {
            for (int column = colOffset; column <= mapSize.first - 1; column += 2) {
                if (mapVectorTiles[column + (row * (mapSize.first))] > 0) {
                    int clusterCount = GetAdjacentWalls(column, row, scopeX, scopeY);
                    if (clusterCount <= (belowCount - 1)) {
                        clusterCount = GetContiguousFilled(column, row);
                        if (clusterCount >= belowCount) {
                            break;
                        }
                    }
                }
            }
        }
        scanCompleted = true;
    }
}

int CavernMapGenerator::PlaceWallLogic(int x, int y) {
    int numWalls = GetAdjacentWalls(x, y, 1, 1);

    if (mapVectorTiles[x + (y * (mapSize.first))] == 1) { //((*mapTiles)[TileKey("Row", std::make_pair(x, y))] == 1)
        if (numWalls >= 4) {
            return 1;
        }
        return 0;
    } else {
        if (numWalls >= 5) {
            return 1;
        }
    }
    return 0;
}

int CavernMapGenerator::GetContiguousFilled(int x, int y) {
    int res = 0;
    int columnExtent = 0;
    int rowExtent = 0;
    for (int row = y; row <= mapSize.second - 1; row++) {
        if (mapVectorTiles[x + (row * (mapSize.first))] >= 1) {
           columnExtent++; 
        } else
            break;
    }
    for (int col = x; col <= mapSize.first - 1; col++) {
        
    }
    return res;
}

int CavernMapGenerator::GetAdjacentWalls(int x, int y, int scopeX, int scopeY) {
    int startX = x - scopeX;
    int startY = y - scopeY;
    int endX = x + scopeX;
    int endY = y + scopeY;

    int iX = startX;
    int iY = startY;

    int wallCounter = 0;

    for (iY = startY; iY <= endY; iY++) {
        for (iX = startX; iX <= endX; iX++) {
            if (!(iX == x && iY == y)) {
                if (IsWall(iX, iY)) {
                    wallCounter += 1;
                }
            }
        }
    }
    return wallCounter;
}

bool CavernMapGenerator::IsWall(int column, int row) {
    if (IsOutOfBounds(column, row)) {
        return true;
    }

    if (mapVectorTiles[column + (row * (mapSize.first))] == 1) {
        return true;
    }

    if (mapVectorTiles[column + (row * (mapSize.first))] == 0) {
        return false;
    }
    return false;
}

bool CavernMapGenerator::IsOutOfBounds(int x, int y) {
    if ((x < 0) || (x > mapSize.first) || (y < 0)) {
        return true;
    }
    return false;
}

void CavernMapGenerator::RandomFillMap() {
    int mapMiddle = 0; // Temp variable
    //(*mapTiles)[TileKey("Row", std::make_pair(column, row))]
    for (int row = 0; row < mapSize.second; row++) {
        for (int column = 0; column < mapSize.first; column++) {
            // If coordinates lie on the edge of the map
            // (creates a border)
            if (column == 0) {
                mapVectorTiles[column + (row * (mapSize.first))] = 1;
            } else if (row == 0) {
                mapVectorTiles[column + (row * (mapSize.first))] = 1;
            } else if (column == mapSize.first - 1) {
                mapVectorTiles[column + (row * (mapSize.first))] = 1;
            } else if (row == mapSize.second - 1) {
                mapVectorTiles[column + (row * (mapSize.first))] = 1;
            }// Else, fill with a wall a random percent of the time
            else {
                mapMiddle = (mapSize.second / 2);
                if (row == mapMiddle) {
                    mapVectorTiles[column + (row * (mapSize.first))] = 0;
                } else {
                    mapVectorTiles[column + (row * (mapSize.first))] = RandomPercent(wallsRatio);
                }
            }
        }
    }
}

int CavernMapGenerator::RandomPercent(float ratio) {
    if (ratio >= randGen.getDoubleInRange(0.0, 1.0)) {
        return 1;
    }
    return 0;
}

int CavernMapGenerator::CombineValue(int value1, int value2, int operation) {
    int res = 0;
    if (operation == 0) {
        if ((value1 == 1) && (value1 == value2))
            res = 1;
    }
    if (operation == 1) {
        if ((value1 == 1) || (value2 == 1))
            res = 1;
    }
    if (operation == 2) {
        if ((value1 != value2))
            res = 1;
    }
    return res;
}

std::string CavernMapGenerator::MapToString() {
    std::stringstream ss;

    if (displayHeader) {
        ss << "Width: " << mapSize.first << " ";
        ss << "tHeight: " << mapSize.second << " ";
    }
    std::vector<std::string> mapSymbols;
    mapSymbols.push_back(".");
    mapSymbols.push_back("#");
    mapSymbols.push_back("+");

    for (int row = 0; row < mapSize.second; row++) {
        for (int column = 0; column < mapSize.first; column++) {
            int tileValue = mapVectorTiles[column + (row * (mapSize.first))];
            ss << mapSymbols[tileValue];
            //returnString += mapSymbols[Map[column,row]];
        }
        ss << std::endl;
    }
    return ss.str();
}

void CavernMapGenerator::BlankMap() {
    for (int row = 0; row < mapSize.second; row++) {
        for (int column = 0; column < mapSize.first; column++) {
            mapVectorTiles[column + (row * (mapSize.first))] = 0;
        }
    }
}

std::vector<int> CavernMapGenerator::GetTileVector() {
    std::vector<int> res = mapVectorTiles;
    return res;
}

void CavernMapGenerator::WriteMapFile(std::string workPath, std::string specFile) {
    //Get the working path and use mapName as the significant part of the file name
    //std::string workPath = ExtraHelper::getWorkingPath();
    std::string mapFileName = ExtraHelper::combineFilePath(workPath, mapName + ".map");
    if (specFile.length() >= 2)
        mapFileName = ExtraHelper::combineFilePath(workPath, specFile);
    std::ofstream outfile((const char*) mapFileName.c_str(), std::ofstream::out);
    outfile << "[" << mapName << "]::(" << mapSize.second << "," << mapSize.first << ")" << std::endl;
    for (int row = 0; row < mapSize.second; row++) {
        outfile << "(" << row << ") = ";
        for (int column = 0; column < mapSize.first; column++) {
            outfile << mapVectorTiles[column + (row * (mapSize.first))];
        }
        outfile << std::endl;
    }
    outfile.close();
}

void CavernMapGenerator::SetMapSize(std::pair<int, int> mapSize) {
    this->mapSize = mapSize;
    mapVectorTiles.reserve(mapSize.first * mapSize.second);
    for (int x = 0; x < mapSize.first; x++) {
        for (int y = 0; y < mapSize.second; y++) {
            mapVectorTiles.push_back(0);
        }
    }
}

std::pair<int, int> CavernMapGenerator::GetMapSize() const {
    return mapSize;
}

void CavernMapGenerator::SetWallsRatio(float wallsRatio) {
    this->wallsRatio = wallsRatio;
}

float CavernMapGenerator::GetWallsRatio() const {
    return wallsRatio;
}

void CavernMapGenerator::SetMapName(std::string mapName) {
    this->mapName = mapName;
}

std::string CavernMapGenerator::GetMapName() const {
    return mapName;
}
