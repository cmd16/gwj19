/* 
 * File:   LevelGeneratorEngine.cpp
 * Author: raoul
 * 
 * Created on 25 March 2020, 15:22
 */

#include "LevelGeneratorEngine.h"

LevelGeneratorEngine::LevelGeneratorEngine(int width, int height) {
    cavernMapGen = std::make_unique<CavernMapGenerator>();
}

LevelGeneratorEngine::LevelGeneratorEngine(const LevelGeneratorEngine& orig) {
}

LevelGeneratorEngine::~LevelGeneratorEngine() {
}

void LevelGeneratorEngine::setFillDistributionRange(float min, float max) {
    
}

void LevelGeneratorEngine::setProductionFile(std::string section, std::string fileName) {
    
}

