/* 
 * File:   ConnectedComponentsFinder.cpp
 * Author: raoul
 * 
 * Created on 22 March 2020, 14:56
 */

#include "ConnectedComponentsFinder.h"
#include <algorithm>
#include <iostream>
#include <stdlib.h>
//#include "imghelpers.h"
#include <utility>
#include <map>
#include "directions.h"

// get the root node (the representative) associated to a node 

ConnectedComponentsFinder::cnode* ConnectedComponentsFinder::FindRep(ConnectedComponentsFinder::cnode *n) {
    while (n -> parent != n) {
        n = n -> parent;
    }
    return n;
}

void ConnectedComponentsFinder::Union(cnode *x, cnode *y) {
    cnode *xRep = FindRep(x);
    cnode *yRep = FindRep(y);
    yRep -> parent = xRep;
}

// create a link between two labels 

void ConnectedComponentsFinder::Union(uint lab1, uint lab2, std::map<uint, cnode *>& labToNode) {
    cnode *x = labToNode[lab1];
    cnode *y = labToNode[lab2];
    Union(x, y);
}

// create new label (and add it to the map)

void ConnectedComponentsFinder::AddLabel(uint& label, std::map<uint, cnode *>& labToNode) {
    cnode *toAdd = new cnode(label);
    labToNode[label] = toAdd;
    ++label;
}

// Second pass of the Union-Find labeling algorithm. Resolves 
// the equivalences of labels. 
// Modifies res by reference to avoid allocating extra memory. 

void ConnectedComponentsFinder::SecondPass(std::map<uint, cnode *>& labToNode, std::vector<uint>& res, uint xLen, uint yLen, uint maxLabel) {
    // decide on the final labels
    std::map<uint, uint> equivClass;
    for (int i = 0; i < (int) maxLabel; ++i) {
        equivClass[i] = FindRep(labToNode[i]) -> label;
    }
    // second pass
    for (int i = 0; i < (int) yLen; ++i) {
        for (int j = 0; j < (int) xLen; ++j) {
            res[i * xLen + j] = equivClass[res[i * xLen + j]];
        }
    }
}

// First pass of the Union-Find labeling algorithm. Attributes labels
// to zones in a forward manner (i.e. by looking only at a subset of the
// neighbors). 

ConnectedComponentsFinder::ConnectedComponentsFinder(std::vector<int> input) {
    ConvertIntVector(input);
}

ConnectedComponentsFinder::ConnectedComponentsFinder(std::vector<int> input, int width, int height) {
    conHeight = height;
    conWidth = width;
    ConvertIntVector(input);
}

ConnectedComponentsFinder::ConnectedComponentsFinder(std::vector<uint> input, int width, int height) {
    conHeight = height;
    conWidth = width;
    inputVertices = input;
}

ConnectedComponentsFinder::ConnectedComponentsFinder(const ConnectedComponentsFinder& orig) {
}

ConnectedComponentsFinder::~ConnectedComponentsFinder() {
}

void ConnectedComponentsFinder::SetDimension(int width, int height) {
    conHeight = height;
    conWidth = width;
}

void ConnectedComponentsFinder::ConvertIntVector(std::vector<int> input) {
    for (int val : input) {
        inputVertices.push_back((uint) val);
    }
}

void ConnectedComponentsFinder::FindConnectedComponentsInput() {
    outVertices = std::make_unique<std::vector<uint>>();
    outVertices->reserve(conWidth * conHeight);
    FindConnectedComponents(inputVertices, conWidth, conHeight, 8);
}

std::vector<uint> ConnectedComponentsFinder::GetOutVertices() {
    std::vector<uint> resultOutVec = *outVertices;
    return resultOutVec;
}

// The Union-Find labeling algorithm. Works in two passes and uses a Union-Find
// data structure to resolve the equivalences between labels. 

void ConnectedComponentsFinder::FindConnectedComponents(const std::vector<uint>& img/*, std::vector<uint>& res*/, uint xLen, uint yLen, uint connectivity) {
    uint currLabel = 0;
    std::map<uint, cnode *> labToNode;
    // first pass: assign labels to different zones
    //res.reserve(img.size());
    outVertices->clear();
    for (int ix = 0; ix < (xLen * yLen); ix++)
        outVertices->push_back(0);
    (*outVertices)[0] = currLabel;
    AddLabel(currLabel, labToNode);
    if (connectivity == 4) {
        FirstPass<4>(img, *outVertices, xLen, yLen, labToNode, currLabel);
    } else if (connectivity == 8) {
        FirstPass<8>(img, *outVertices, xLen, yLen, labToNode, currLabel);
    } else {
        std::cout << "This is not a valid connectivity! Exiting ..." << std::endl;
        exit(1);
    }
    // second pass: merge classes if necessary
    SecondPass(labToNode, *outVertices, xLen, yLen, currLabel);
}
