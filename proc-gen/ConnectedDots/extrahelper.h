#ifndef EXTRA_HELPER_H
#define EXTRA_HELPER_H

#include <vector>
#include <sstream>
#include <cstring>
#include <sys/stat.h>
#include <unistd.h>

    class ExtraHelper {
    public:
        //static std::string printVector2(Vector2 vec, std::string tag = "");
        
        static std::vector<std::string> readFileAsVector(std::string dataFile);
        
        static std::string getWorkingPath();
        
        static std::string combineFilePath(std::string aPath, std::string aFile);
        
        static std::string printShortTimestamp();
        
        template <typename T>
        static std::string printDebugVector(std::string tag, int lineFieldCount, std::vector<T> vectorList) {
            std::stringstream stringreader;
            int ix = 0;
            for (T item : vectorList) {
                if (lineFieldCount > 0) {
                    if (ix < lineFieldCount)
                        stringreader << item << "\t";
                    else {
                        ix = 0;
                        stringreader << item << std::endl;
                    }
                } else
                    stringreader << item << std::endl;
                ix++;
            }
            return stringreader.str();
        }
        
        static std::vector<std::string> SplitTextOn(std::string mainStr, std::string split, bool includeSplit = false);
        
        static inline bool FileExists(std::string aFile) {
            struct stat buffer;   
            return (stat (aFile.c_str(), &buffer) == 0);
        }        
	
    };

#endif
