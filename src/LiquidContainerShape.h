/* 
 * File:   LiquidContainerShape.h
 * Author: raoul
 *
 * Created on 17 March 2020, 23:26
 */

#ifndef LIQUIDCONTAINERSHAPE_H
#define LIQUIDCONTAINERSHAPE_H

/* This class purpose is to generate a procedural shape based on the terrain
  and liquid properties provided to it, to generate a boundary container shape for
  a given liquid mass to stay in (be contained) */

class LiquidContainerShape {
public:
    LiquidContainerShape();
    LiquidContainerShape(const LiquidContainerShape& orig);
    virtual ~LiquidContainerShape();
private:

};

#endif /* LIQUIDCONTAINERSHAPE_H */

