#include "MazeHolder.h"
#include "extrahelper.h"
#include <sstream>

using namespace godot;

void MazeHolder::_register_methods() {
    register_method((char*) "_ready", &MazeHolder::_ready);
    register_method((char*) "setdimension", &MazeHolder::setdimension);
    register_method((char*) "generatelevel", &MazeHolder::generatelevel);
    register_method((char*) "generateautolevel", &MazeHolder::generateautolevel);
    register_method((char*) "dumpdebug", &MazeHolder::dumpdebug);
    register_method((char*) "cleardebug", &MazeHolder::cleardebug);
    register_method((char*) "autoleveldatamap", &MazeHolder::autoleveldatamap);
    register_method((char*) "getgateportalcoordinates", &MazeHolder::getgateportalcoordinates);
    register_method((char*) "getkeyplacementcoordinates", &MazeHolder::getkeyplacementcoordinates);
}

void MazeHolder::_init() {
}

MazeHolder::MazeHolder(std::string resourceScene) {
    mapTileUsageRatios = std::make_unique<std::unordered_map <std::string, std::pair<float, float>> >();
    ResourceLoader *rl = ResourceLoader::get_singleton();
    //wallScene = rl->load(resourceScene);
}

MazeHolder::MazeHolder() {
    mapTileUsageRatios = std::make_unique<std::unordered_map <std::string, std::pair<float, float>> >();
}

MazeHolder::~MazeHolder() {
    mazeObject = nullptr;
    /*
    if (mazeNodes) {
        for (int x = 0; x < mazeWidth; x++) {
            delete mazeNodes[x];
        }
        delete mazeNodes;
        mazeNodes = nullptr;
    }
     * */
}

void MazeHolder::AddTileUsage(String tileSection, float proportion, float tolerance) {
    if (mapTileUsageRatios != nullptr) {
        //if (debugGroupMessages->find(group) != debugGroupMessages->end()) {
        std::string strSection = std::string(tileSection.alloc_c_string());
        (*mapTileUsageRatios)[strSection] = std::make_pair(proportion, tolerance);
    }
}

void MazeHolder::setdimension(int cols, int rows) {
    mazeWidth = cols;
    mazeHeight = rows;
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "MazeHolder::setdimension, cols=%d rows=%d \n", cols, rows);
    Godot::print(debugText);
}

void MazeHolder::generatelevel(String mapfile) {
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "MazeHolder::generatelevel, mapfile=%s \n", mapfile.alloc_c_string());
    Godot::print(debugText);
    mazeObject->generateMaze(workPath, mapfile.alloc_c_string()); //"level1world.map"
}

void MazeHolder::generateautolevel() {
    mazeObject->setMapTileSpan(std::make_pair(mazeWidth, mazeHeight));
    mazeObject->generateMaze();
}

void MazeHolder::autoleveldatamap(String mapfile) {
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "MazeHolder::autoleveldatamap, mapfile=%s \n", mapfile.alloc_c_string());
    Godot::print(debugText);
    mazeObject->WriteMapData(workPath, mapfile.alloc_c_string(), true);
}

/*
godot_variant simple_get_data(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args) {
	godot_string data;
	godot_variant ret;
	user_data_struct * user_data = (user_data_struct *) p_user_data;

	api->godot_string_new(&data);
	api->godot_string_parse_utf8(&data, user_data->data);
	api->godot_variant_new_string(&ret, &data);
	api->godot_string_destroy(&data);

	return ret;
}
 * */
Variant MazeHolder::getgateportalcoordinates() {
    Variant res;
    
    String coordinateText = mazeObject->GetItemPlacementCoordinates("gate").c_str();
    res = coordinateText;
    
    return res;
}

Variant MazeHolder::getkeyplacementcoordinates() {
    Variant res;
    
    String coordinateText = mazeObject->GetItemPlacementCoordinates("keys").c_str();
    res = coordinateText;
    
    return res;
}

void MazeHolder::dumpdebug(String section) {
    std::string paramSection = std::string(section.alloc_c_string());
    if (paramSection.length() >= 2) {
            char debugText[1024];
            memset(debugText, 0, 1024);
            snprintf(debugText, 1023, "MazeHolder::dumpdebug, section=%s \n", section.alloc_c_string());
        Godot::print(debugText);
        mazeObject->DumpDebugMessages(paramSection);
    }
}

void MazeHolder::cleardebug(String section) {
    std::string paramSection = std::string(section.alloc_c_string());
    if (paramSection.length() >= 2) {
        mazeObject->ClearDebugMessages(paramSection);
    }
}

void MazeHolder::_ready() {
    workPath = ExtraHelper::getWorkingPath();
    mazeObject = std::make_unique<MazeGenerator>();
}

std::string MazeHolder::createNameValue(int x, int y, std::string tileCategory) {
    std::stringstream ss;
    if (tileCategory.length() <= 0)
        ss << "Wall#" << x << "_" << y;
    else
        ss << tileCategory << "#" << x << "_" << y;
    return ss.str();
}


