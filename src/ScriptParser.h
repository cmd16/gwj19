/* 
 * File:   ScriptParser.h
 * Author: raoul
 *
 * Created on 03 March 2020, 23:35
 */
#ifndef SCRIPT_PARSER_H
#define SCRIPT_PARSER_H

#include <utility>
#include <vector>
#include <string>
#include <Godot.hpp>

namespace godot {

    class ScriptParser {
    public:
        ScriptParser();
        ScriptParser(const ScriptParser& orig);
        virtual ~ScriptParser();
        
        void addKeyTokens(std::vector<std::string> keyTokens);
        std::vector<std::string> parseSimpleTokens(std::string input);
        std::vector<std::pair<int, std::string>> getPositionTokens();
    private:
        std::vector<std::string> keyTokensList;
    };
}

#endif /* SCRIPT_PARSER_H */

