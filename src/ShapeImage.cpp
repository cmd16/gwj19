#include "ShapeImage.h"
#include <sstream>

using namespace godot;

void ShapeImage::_register_methods() {
    register_method((char*) "_ready", &ShapeImage::_ready);
}

void ShapeImage::_init() {
}

ShapeImage::ShapeImage() {

}

ShapeImage::ShapeImage(std::string imageResource) {
    ResourceLoader *rl = ResourceLoader::get_singleton();
    if (rl != nullptr)
        shapeImageResource = rl->load(imageResource.c_str());
}

ShapeImage::~ShapeImage() {

}

void ShapeImage::_ready() {
    //Read the parameter specification
    std::string inputScript = "rect:100:150:20:20";
    auto inputTokens = scriptParser.parseSimpleTokens(inputScript);
    if (inputTokens.size() > 0) {
        
    }
}
