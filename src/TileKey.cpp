/* 
 * File:   TileKey.cpp
 * Author: raoul
 * 
 * Created on 21 March 2020, 00:02
 */

#include "TileKey.h"

TileKey::TileKey() {
}

TileKey::TileKey(std::string id, std::pair<int, int> aPosition) {
    tileId = id;
    position = aPosition;
}

TileKey::TileKey(const TileKey& orig) {
}

TileKey::~TileKey() {
}

bool TileKey::operator==(const TileKey &other) const {
    return ( (tileId == other.tileId) && (position == other.position));
}

std::size_t TileKey::operator()(const TileKey& k) const// noexcept
{
    using std::size_t;
    using std::hash;
    using std::string;

    // Compute individual hash values for first,
    // second and third and combine them using XOR
    // and bit shifting:

    return ((hash<string>()(k.tileId)
            ^ (hash<int>()(k.position.first) << 1)) >> 1)
            ^ (hash<int>()(k.position.second) << 1);
}

void TileKey::SetPosition(std::pair<int, int> position) {
    this->position = position;
}

std::pair<int, int> TileKey::GetPosition() const {
    return position;
}

void TileKey::SetTileId(std::string tileId) {
    this->tileId = tileId;
}

std::string TileKey::GetTileId() const {
    return tileId;
}

TileKey TileKey::GenerateTileKey(std::string id, std::pair<int, int> aPosition) {
    return TileKey(id, aPosition);
}

namespace std {
    
    std::ostream& operator<< (std::ostream &out, const TileKey& k) {
        out << k.tileId << std::string("::");
        //out << std::string("(") << k.position.first << std::string(",") << k.position.second << std::string(")");
        return out;
    }

    /*
    template <>
    struct hash<TileKey> {

        std::size_t operator()(const TileKey& k) const// noexcept
        {
            // Compute individual hash values for first,
            // second and third and combine them using XOR
            // and bit shifting:
            return ((std::hash<string>()(k.tileId)
                    ^ (std::hash<int>()(k.position.first) << 1)) >> 1)
                    ^ (std::hash<int>()(k.position.second) << 1);
        }
    };
     * */
}