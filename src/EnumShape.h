#ifndef ENUM_SHAPE_H
#define ENUM_SHAPE_H

#include <Godot.hpp>

enum EnumerationShape {
    SHAPE_NONE,
    SHAPE_TRIANGLE,
    SHAPE_RECTANGLE,
    SHAPE_OVAL,
    SHAPE_POLYGON
};

class EnumShape {
    static EnumerationShape castFromString(std::string strValue);
    static std::string print(EnumerationShape sShape, std::string tag);
    static std::string toString(EnumerationShape sShape);
};
#endif
