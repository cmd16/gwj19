/* 
 * File:   VectorUtilities.h
 * Author: raoul
 *
 * Created on 23 March 2020, 14:55
 */

#include <vector>

#ifndef VECTORUTILITIES_H
#define VECTORUTILITIES_H

class VectorUtilities {
public:
    template <typename T>
    static std::vector<T> FindWhere(std::vector<T> list) {
        std::vector<T> res;
        return res;
    }
    
    template <typename T>
    static std::vector<int> FindOccurencesCount(std::vector<T> list) {
        std::vector<int> res;
        return res;
    }
private:
    
};

#endif /* VECTORUTILITIES_H */

