#ifndef GDEXAMPLE_H
#define GDEXAMPLE_H

#include <Godot.hpp>
#include <sstream>
#include <fstream>
#include <ctime>
#include "extrahelper.h"

using namespace godot;

static const std::string FileSystemSeparator = "/";

std::string ExtraHelper::printVector2(Vector2 vec, std::string tag) {
    std::stringstream ss;
    ss << tag << vec.x << "," << vec.y << std::endl;
    return ss.str();
}

std::string ExtraHelper::getWorkingPath() {
    char workDirChars[2048];
    memset(workDirChars, 0, 2048);
    getcwd(workDirChars, 2047);
    return workDirChars;
}

std::string ExtraHelper::combineFilePath(std::string aPath, std::string aFile) {
    return aPath + FileSystemSeparator + aFile;
}

std::string ExtraHelper::printShortTimestamp() {
    time_t rawtime;
    struct tm* timeinfo;
    char buffer[128];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof (buffer), "%d%m%Y#%H%M%S", timeinfo);
    return buffer;
}

std::vector<std::string> ExtraHelper::SplitTextOn(std::string mainStr, std::string split, bool includeSplit) {
    std::vector<std::string> res;
    if ( (mainStr.length() > 0) && (mainStr.length() > split.length()) ) {
        std::string workStr = mainStr;
        std::size_t found = 0;
        while ( (found >= 0) && (found < workStr.length()) ) {
            found = workStr.find(split);
            if ( (found >= 0) && (found < workStr.length()) ) {
                std::string foundStr = workStr.substr(0, found);
                workStr = workStr.substr(found + split.length());
                res.push_back(foundStr);
            }
        }
        if (workStr.length() > 0)
            res.push_back(workStr);
    }
    return res;
}

std::vector<std::string> ExtraHelper::readFileAsVector(std::string dataFile) {
    std::vector<std::string> res;
    std::string line;
    std::ifstream infile((const char*) dataFile.c_str(), std::ifstream::in);
    while (getline(infile, line, '\n')) {
        res.push_back(line);
    }
    infile.close();
    return res;
}

#endif
