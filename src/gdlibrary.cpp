#include "ShapeImage.h"
//#include "MultiAdapterDraw.h"
#include "MazeHolder.h"
#include "GennerTileMap.h"

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);

    godot::register_class<godot::ShapeImage>();
    //godot::register_class<godot::MultiAdapterDraw>();
    godot::register_class<godot::MazeHolder>();
    godot::register_class<godot::GennerTileMap>();
}
