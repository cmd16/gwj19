/* 
 * File:   MazeGenerator.cpp
 * Author: raoul
 * 
 * Created on 03 March 2020, 23:35
 */

#include <memory>
#include <algorithm>
#include <sstream>
#include <ctime>
#include "extrahelper.h"
#include "MazeGenerator.h"
#include "RandomUtility.h"
#include "ConnectedComponentsFinder.h"
#include "imghelpers.h"

using namespace godot;

MazeGenerator::MazeGenerator() {
    mapSelectionTiles = std::make_unique<std::unordered_map <std::string, TileInfo> >();
    debugGroupMessages = std::make_unique<std::unordered_map <std::string, std::stringstream> >();
    mapPlacedTiles = std::make_unique<std::unordered_map <TileKey, TileInfo, TileKeyHashFunc> >();
}

MazeGenerator::MazeGenerator(int numHorizontalTiles, int numVerticalTiles) {
    mapTileSpan.first = numHorizontalTiles;
    mapTileSpan.second = numVerticalTiles;
    mapSelectionTiles = std::make_unique<std::unordered_map <std::string, TileInfo> >();
    debugGroupMessages = std::make_unique<std::unordered_map <std::string, std::stringstream> >();
    mapPlacedTiles = std::make_unique<std::unordered_map <TileKey, TileInfo, TileKeyHashFunc> >();
}

MazeGenerator::MazeGenerator(const MazeGenerator& orig) {
}

MazeGenerator::~MazeGenerator() {
}

void MazeGenerator::LogDebugMessage(std::string group, std::string message) {
    if ((debugGroupMessages != nullptr) && (group.length() >= 1)) {
        /*
        if (debugGroupMessages->find(group) != debugGroupMessages->end()) {
        } else {
            //std::unique_ptr<std::stringstream> ssMesg = std::make_unique<std::stringstream>();
            //(*debugGroupMessages)[group] = ssMesg;
        }
         */
        (*debugGroupMessages)[group] << message << std::endl;
    }
}

void MazeGenerator::DumpDebugMessages(std::string group) {
    if (debugGroupMessages->find(group) != debugGroupMessages->end()) {
        std::string debugText = (*debugGroupMessages)[group].str();
        Godot::print(debugText.c_str());
    } else
        Godot::print("MazeGenerator::DumpDebugMessages() -- no group found");
}

void MazeGenerator::ClearDebugMessages(std::string group) {
    if (debugGroupMessages->find(group) != debugGroupMessages->end()) {
        char debugText[1024];
        memset(debugText, 0, 1024);
        snprintf(debugText, 1023, "MazeGenerator::ClearDebugMessages, group=%s \n", group.c_str());
        Godot::print(debugText);
        (*debugGroupMessages)[group].clear();
    }
}

std::vector<int> MazeGenerator::CreateRandomNumberGrid(int width, int height, int minValue, int maxValue) {
    std::vector<int> res;
    RandomUtility randGen;
    for (int iw = 0; iw < width; iw++) {
        for (int ih = 0; ih < height; ih++) {
            res.push_back(randGen.getIntInRange(minValue, maxValue));
        }
    }
    return res;
}

std::string MazeGenerator::GetItemPlacementCoordinates(std::string item) {
    std::string res = "";
    if (item == "gate") {
        return strGateCoordinates;//"gate=(0,0),(1,1)";
    }
    if (item == "keys") {
        return strKeysCoordinates;//"keys=(1,5),(10,10),(20,15)";
    }
    return res;
}

void MazeGenerator::setMapTileSpan(std::pair<int, int> mapTileSpan) {
    this->mapTileSpan = mapTileSpan;
}

std::vector<uint> ConvertIntVector(std::vector<int> input) {
    std::vector<uint> inputVertices;
    for (int val : input) {
        inputVertices.push_back((uint) val);
    }
    return inputVertices;
}

bool MazeGenerator::RegionWithinBounds(uint regionNum, int x, int y, int width, int height, int blocks, std::vector<uint> regionMap) {
    bool res = false;
    uint mapwidth = mapTileSpan.first;
    uint mapheight = mapTileSpan.second;
    std::vector<uint>::iterator iter = regionMap.begin();
    while ((iter = std::find_if(iter, regionMap.end(), [regionNum](uint i) {
            return (i == regionNum);
        })) != regionMap.end()) {
        int idx = std::distance(regionMap.begin(), iter);
        int ccol = idx % mapwidth;
        int crow = idx / mapwidth;
        if ((ccol >= x) && (ccol <= x + width)) {
            if ((crow >= y) && (crow < y + height)) {
                res = true;
                break;
            }
        }
        iter++;
    }
    return res;
}

std::vector<uint> MazeGenerator::GetRegionTileCoordinates(uint regionNum, std::vector<uint> regionMap, int limit) {
    uint mapwidth = mapTileSpan.first;
    uint mapheight = mapTileSpan.second;
    std::vector<uint> res;
    std::vector<uint>::iterator iter = regionMap.begin();
    while ((iter = std::find_if(iter, regionMap.end(), [regionNum](uint i) {
            return (i == regionNum);
        })) != regionMap.end()) {
        int idx = std::distance(regionMap.begin(), iter);
        res.push_back(idx);
        iter++;
    }
    std::sort(res.begin(), res.end());
    if (limit > 0) {
        
    }
    
    return res;
}

void MazeGenerator::WriteCoordinates(std::string workPath, std::string coordFile, std::string regionTag, std::vector<uint> regionTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::string fullPathData = ExtraHelper::combineFilePath(workPath, coordFile);
    std::ofstream outfile((const char*) fullPathData.c_str(), std::ofstream::out);
    std::string regionCoords = WriteCoordinatesString(regionTileCoords);
    outfile << regionCoords << std::endl;
    outfile.close();
}

std::string MazeGenerator::WriteCoordinatesString(std::vector<uint> regionTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::stringstream outgen;
    for (uint tile : regionTileCoords) {
        int ccol = tile % mapwidth;
        int crow = tile / mapwidth;
        outgen << "(" << ccol << "," << crow << ")";        
        outgen << ";";
    }
    return outgen.str();
}

std::string MazeGenerator::WriteCoordinatesMapString(std::unordered_map<uint, std::vector<uint>> mapTileCoords) {
    uint mapwidth = mapTileSpan.first;
    std::stringstream outgen;
    for (std::pair<uint, std::vector<uint>> element : mapTileCoords) {
	uint region = element.first;
        std::vector<uint> coordinatesList = element.second;
        outgen << region << "=[";
        std::string strTileCoordinates = WriteCoordinatesString(coordinatesList);
        outgen << strTileCoordinates;
        outgen << "]$";
    }
    return outgen.str();
}

void MazeGenerator::generateMaze() {
    //Default generator for the level "maze"
    RandomUtility randGmaze;
    randGmaze.setRandomSeed(time(0));
    float range1 = randGmaze.getDoubleInRange(0.41, 0.445); //(0.33, 0.42);
    float range2 = randGmaze.getDoubleInRange(0.42, 0.47); //(0.39, 0.45)
    int blockSliceWidth = randGmaze.getIntInRange(21, 27);
    int blockSliceHeight = randGmaze.getIntInRange(4, 6);
    int bottomBlockSliceWidth = randGmaze.getIntInRange(18, 24);
    int bottomBlockSliceHeight = randGmaze.getIntInRange(6, 9);
    cavernBlobMap1 = std::make_unique<CavernMapGenerator>();
    cavernBlobMap2 = std::make_unique<CavernMapGenerator>();
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "[1]MazeGenerator::generateMaze(), range1=%f, range2=%f \n", range1, range2);
    Godot::print(debugText);
    if ((cavernBlobMap1 != nullptr) && (cavernBlobMap1 != nullptr)) {
        cavernBlobMap1->SetMapName("platform1");
        cavernBlobMap2->SetMapName("platform2");
        cavernBlobMap1->SetMapSize(mapTileSpan);
        cavernBlobMap2->SetMapSize(mapTileSpan);
        cavernBlobMap1->BlankMap();
        cavernBlobMap2->BlankMap();
        cavernBlobMap1->SetWallsRatio(range1);
        cavernBlobMap2->SetWallsRatio(range2);
        cavernBlobMap1->RandomFillMap();
        cavernBlobMap2->RandomFillMap();
        cavernBlobMap1->GenerateMap(true);
        cavernBlobMap2->GenerateMap(true);
        cavernBlobMap1->RandomSelectFillRows(3, 5, 4, 5, 0);
        cavernBlobMap2->RandomSelectFillRows(3, 4, 2, 3, 0);
        cavernBlobMap1->RandomSelectFillColumns(2, 4, 3, 4, 0);
        cavernBlobMap2->RandomSelectFillColumns(1, 3, 3, 4, 0);
        cavernBlobMap2->SliceBlock(0.34, 0.64, bottomBlockSliceWidth, bottomBlockSliceHeight, 0);
        cavernBlobMap1->Overlay(*cavernBlobMap2, 1);
        cavernBlobMap1->RandomSelectFillRows(1, 2, 3, 4, 0);
        cavernBlobMap1->RandomSelectFillColumns(2, 3, 2, 3, 0);
        cavernBlobMap1->SliceBlock(0.21, 0.45, blockSliceWidth, blockSliceHeight, 0);
        uint xLen = mapTileSpan.first;
        uint yLen = mapTileSpan.second;
        auto vecTiles = cavernBlobMap1->GetTileVector();
        vecGeneratedMap = ConvertIntVector(vecTiles);
        ConnectedComponentsFinder componentFinder(vecTiles, mapTileSpan.first, mapTileSpan.second);
        componentFinder.FindConnectedComponentsInput();
        auto resOut = componentFinder.GetOutVertices();
        //std::string debugVectorOut = ExtraHelper::printDebugVector<uint>("", 40, resOut);
        //std::cout << "Result map = " << debugVectorOut << std::endl;        
        //Get the set of unique labels in the resOut map that is the output of the connected component finder
        std::vector<uint> filterFields = resOut;
        std::vector<uint> limitEraseRegions;
        std::unordered_map<uint, int> regionCountMap;
        std::sort(filterFields.begin(), filterFields.end());
        filterFields.erase(std::unique(filterFields.begin(), filterFields.end()), filterFields.end());
        for (uint regionNum : filterFields) {
            //For each label determine the find count of that label in the vector map "resOut"
            std::vector<uint> filterRegionCount(resOut.size());
            auto it = std::copy_if(resOut.begin(), resOut.end(), filterRegionCount.begin(), [regionNum](uint i) {
                return (i == regionNum);
            });
            filterRegionCount.resize(std::distance(filterRegionCount.begin(), it));
            regionCountMap[regionNum] = filterRegionCount.size();
            std::cout << "Region[" << regionNum << "] = " << filterRegionCount.size() << std::endl;
            if (filterRegionCount.size() <= eraseBlockThreshold) {
                limitEraseRegions.push_back(regionNum);
            }
        }
        for (uint removeregion : limitEraseRegions) {
            std::vector<uint>::iterator iter = resOut.begin();
            while ((iter = std::find_if(iter, resOut.end(), [removeregion](uint i) {
                    return (i == removeregion);
                })) != resOut.end()) {
            int idx = std::distance(resOut.begin(), iter);
            vecGeneratedMap[idx] = 0;
            std::cout << idx;
            iter++;
        }
        }

        //Now find the regions again on the cleaned up "vecGeneratedMap"
        ConnectedComponentsFinder componentFinder2(vecGeneratedMap, mapTileSpan.first, mapTileSpan.second);
        componentFinder2.FindConnectedComponentsInput();
        auto resOut2 = componentFinder2.GetOutVertices();
        std::string descriptor = ExtraHelper::printShortTimestamp();
        std::string outFile = descriptor + "_out.map";
        RelabelImg(resOut2, xLen, yLen);
        WriteImgToFile(outFile, resOut2, xLen, yLen);
        filterFields = resOut2;
        regionCountMap.clear();
        std::sort(filterFields.begin(), filterFields.end());
        filterFields.erase(std::unique(filterFields.begin(), filterFields.end()), filterFields.end());
        for (uint regionNum : filterFields) {
            //For each label determine the find count of that label in the vector map "resOut"
            std::vector<uint> filterRegionCount(resOut.size());
            auto it = std::copy_if(resOut.begin(), resOut.end(), filterRegionCount.begin(), [regionNum](uint i) {
                return (i == regionNum);
            });
            filterRegionCount.resize(std::distance(filterRegionCount.begin(), it));
            regionCountMap[regionNum] = filterRegionCount.size();
            //std::cout << "Region[" << regionNum << "] = " << filterRegionCount.size() << std::endl;
        }
        
        //find the best region for the placement of the portal
        uint regionPortal = 0;
        for (uint regionNum : filterFields) {
            if (regionNum <= 1)
                continue;
            if (RegionWithinBounds(regionNum, 24, 25, (xLen * 0.52  ), (yLen * 0.35), 14, resOut2)) {
                //Found a suitable centre "origin" block
                //Find the top surface coordinates of this block
                std::vector<uint> portalTileCoords = GetRegionTileCoordinates(regionNum, resOut2);
                //std::sort(portalTileCoords.begin(), portalTileCoords.end());
                strGateCoordinates = WriteCoordinatesString(portalTileCoords);                
                std::string workPathGen = ExtraHelper::getWorkingPath();
                outFile = "gencavern_gatezone.crd";
                std::string regionTag = "";
                WriteCoordinates(workPathGen, outFile, regionTag, portalTileCoords);                
                break;
            }
        }
        
        //Find the multiple regions for the keys placement
        int keyCounter = 0;
        std::unordered_map<uint, std::vector<uint>> regionKeyCoordsMap;
        for (uint regionNum : filterFields) {
            if ( (regionNum <= 1) || (regionNum == regionPortal) )
                continue;
            if (RegionWithinBounds(regionNum, 11, 12, (xLen * 0.72  ), (yLen * 0.62), 9, resOut2)) {
                //Find the top surface coordinates of this block
                std::vector<uint> keyTileCoords = GetRegionTileCoordinates(regionNum, resOut2);
                regionKeyCoordsMap[regionNum] = keyTileCoords;
                keyCounter++;
                break;
            }
        }
        strKeysCoordinates = WriteCoordinatesMapString(regionKeyCoordsMap);

        //VectorUtilities::FindMatching(resOut);
        memset(debugText, 0, 1024);
        snprintf(debugText, 1023, "[1]MazeGenerator::generateMaze(), Map generation complete \n");
        Godot::print(debugText);
    }
}

void MazeGenerator::generateMaze(std::string workPath, std::string dataFile) {
    if (dataFile.length() > 0) {
        char debugText[1024];
        memset(debugText, 0, 1024);
        snprintf(debugText, 1023, "[2]MazeGenerator::generateMaze(), workPath=%s, dataFile=%s \n", workPath.c_str(), dataFile.c_str());
        Godot::print(debugText);
        std::string fullPathData = ExtraHelper::combineFilePath(workPath, dataFile);
        if (ExtraHelper::FileExists(fullPathData)) {
            std::vector<std::string> mapLineEntries = ExtraHelper::readFileAsVector(fullPathData);
            memset(debugText, 0, 1024);
            snprintf(debugText, 1023, "MazeGenerator::generateMaze(), #map entries=%ld\n", mapLineEntries.size());
            Godot::print(debugText);
            if (mapLineEntries.size() > 0) {
                for (std::string entry : mapLineEntries) {
                    ProcessMapEntry(entry);
                }
            }
        } else {
            memset(debugText, 0, 1024);
            snprintf(debugText, 1023, "MazeGenerator::generateMaze(), dataFile=%s does not exist\n", fullPathData.c_str());
            Godot::print(debugText);
        }
    }
}

void MazeGenerator::WriteMapFile(std::string workPath, std::string dataFilemap, std::vector<uint> mapData) {
    std::string fullPathData = ExtraHelper::combineFilePath(workPath, dataFilemap);
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "MazeGenerator::WriteMapFile, workPath=%s, dataFilemap=%s\n", workPath.c_str(), dataFilemap.c_str());
    Godot::print(debugText);
    std::ofstream outfile((const char*) fullPathData.c_str(), std::ofstream::out);
    uint width = mapTileSpan.first;
    uint height = mapTileSpan.second;
    std::string mapName = dataFilemap;
    outfile << "[" << mapName << "]::(" << height << "," << width << ")" << std::endl;
    for (int row = 0; row < height; row++) {
        outfile << "(" << row << ") = ";
        for (int column = 0; column < width; column++) {
            outfile << mapData[column + (row * width)];
        }
        outfile << std::endl;
    }
    outfile.close();
}

void MazeGenerator::WriteMapData(std::string workPath, std::string dataFilemap, bool writeIntermediate) {
    std::string fullPathData = ExtraHelper::combineFilePath(workPath, dataFilemap);
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "MazeGenerator::WriteMapData, workPath=%s, dataFilemap=%s\n", workPath.c_str(), dataFilemap.c_str());
    Godot::print(debugText);
    if ((writeIntermediate) && (cavernBlobMap1 != nullptr)) {
        cavernBlobMap1->WriteMapFile(workPath);
        cavernBlobMap2->WriteMapFile(workPath);
    }
    WriteMapFile(workPath, dataFilemap, vecGeneratedMap);
    //cavernBlobMap1->WriteMapFile(workPath, dataFilemap);
}

void MazeGenerator::ProcessMapEntry(std::string mapEntry) {
    char debugText[2048];
    if (mapEntry.length() >= 4) {
        memset(debugText, 0, 2048);
        snprintf(debugText, 2047, "MazeGenerator::ProcessMapEntry(), mapEntry=%s \n", mapEntry.c_str());
        LogDebugMessage("datafile", debugText);
        auto sectionFields = ExtraHelper::SplitTextOn(mapEntry, "="); //2 parts -> Section ID (Row) and array of data for that section
        if (sectionFields.size() >= 2) {
            std::string sectionID = sectionFields[0];
            memset(debugText, 0, 2048);
            snprintf(debugText, 2047, "MazeGenerator::ProcessMapEntry(), sectionID=%s \n", sectionID.c_str());
            LogDebugMessage("datafile", debugText);
            auto cellValues = ExtraHelper::SplitTextOn(sectionFields[1], ",");
            std::string cellDataDebug = ExtraHelper::printDebugVector<std::string>("Section = " + sectionID, cellValues);
            LogDebugMessage("datafile", cellDataDebug);
            placeTileFromInput(cellValues);
            //Convert these string values into "TileInfo" ??
        }
    }
}

bool MazeGenerator::isWall(int column, int row) {
    bool res = false;
    if ((column >= 1) && (row >= 1)) {

    }
    return res;
}

std::pair<int, int> MazeGenerator::getUnitTileDimension() {
    return unitTileDimension;
}

std::pair<int, int> MazeGenerator::getMapTileSpan() {
    return mapTileSpan;
}

std::string MazeGenerator::getTileID(int column, int row) {
    std::string res = "";
    return res;
}

void MazeGenerator::setUnitTileDimension(std::pair<int, int> tileDim) {
    unitTileDimension = tileDim;
}

void MazeGenerator::addTileDescription(std::string tileID, std::pair<int, int> unitSize, std::string resourceImage) {
    if (tileID.length() >= 2) {
        //mapSelectionTiles
    }
}

bool MazeGenerator::tileExists(std::string tileID) {
    bool res = false;
    if ((tileID.length() >= 2) && (mapSelectionTiles != nullptr)) {
        res = mapSelectionTiles->find(tileID) != mapSelectionTiles->end();
    }
    return res;
}

void MazeGenerator::placeTile(std::string tileID, std::pair<int, int> mapPosition) {
    if ((tileID.length() >= 2) && (tileExists(tileID))) {
        TileInfo placeTileInfo = TileInfo::CreateFrom(tileID); // id and a (parse string) :: id -> {dimension, resource, frames ... }
        //(*mapPlacedTiles)[TileKey::GenerateTileKey(tileID, mapPosition)] = placeTileInfo;
    }
}

void MazeGenerator::placeTileFromInput(std::vector<std::string> fields) {
    if (fields.size() >= 2) {
        std::string tileId = fields[0];
        char debugText[2048];
        memset(debugText, 0, 2048);
        snprintf(debugText, 2047, "MazeGenerator::placeTileFromInput(), tileId=%s \n", tileId.c_str());
        LogDebugMessage("datafile", debugText);
        std::pair<int, int> tPosition;
        placeTile(tileId, tPosition);
    }
}

void MazeGenerator::modifyTilesInRow(int row, std::vector<TileInfo> tileArray) {
    if ((row >= 1) && (row <= mapTileSpan.second)) {

    }
}

