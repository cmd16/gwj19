/* 
 * File:   StyleTransition.h
 * Author: raoul
 *
 * Created on 17 March 2020, 23:22
 */

#ifndef STYLETRANSITION_H
#define STYLETRANSITION_H

//#include <vector>
#include <string>
#include <utility>

/* style-name
   type 
   value 
 */

class StyleTransition {
public:
    StyleTransition(std::string styleName);
    StyleTransition(const StyleTransition& orig);
    virtual ~StyleTransition();
    
    int getType();
    std::string getName();
private:
    std::string styleName;
    int styleType;
};

#endif /* STYLETRANSITION_H */

