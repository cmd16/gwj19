#ifndef GDEXAMPLE_H
#define GDEXAMPLE_H

#include <Godot.hpp>
#include <sstream>
#include "EnumShape.h"

using namespace godot;

EnumerationShape castFromString(std::string strValue) {
    EnumerationShape res = EnumerationShape::SHAPE_NONE;
    return res;
}

std::string EnumShape::print(EnumerationShape sShape, std::string tag) {
    std::stringstream ss;
    ss << tag << "" << std::endl;
    return ss.str();
}

std::string EnumShape::toString(EnumerationShape sShape) {
    std::string res = "none";
    switch (sShape) {
        case EnumerationShape::SHAPE_TRIANGLE:
            res = "triangle";
            break;
        case EnumerationShape::SHAPE_RECTANGLE:
            res = "rectangle";
            break;
        case EnumerationShape::SHAPE_OVAL:
            res = "oval";
            break;
        default:
            break;
    }
    return res;
}

#endif
