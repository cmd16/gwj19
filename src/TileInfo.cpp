/* 
 * File:   TileInfo.cpp
 * Author: raoul
 * 
 * Created on 17 March 2020, 00:35
 */

#include "TileInfo.h"

TileInfo::TileInfo() {
    animationImagesMap = std::make_unique<std::unordered_map <int, std::string> >();
}

TileInfo::TileInfo(std::string id) {
    tileID = id;
    animationImagesMap = std::make_unique<std::unordered_map <int, std::string> >();
}

TileInfo::TileInfo(const TileInfo& orig) {
}

TileInfo::~TileInfo() {
}

void TileInfo::SetPixelDimensions(std::pair<int, int> pixelDimensions) {
    this->pixelDimensions = pixelDimensions;
}

std::pair<int, int> TileInfo::GetPixelDimensions() const {
    return pixelDimensions;
}

void TileInfo::SetAnimFrameIndex(int animFrameIndex) {
    this->animFrameIndex = animFrameIndex;
}

int TileInfo::GetAnimFrameIndex() const {
    return animFrameIndex;
}

void TileInfo::SetTileID(std::string tileID) {
    this->tileID = tileID;
}

std::string TileInfo::GetTileID() const {
    return tileID;
}

void TileInfo::AddImageFrame(int frame, std::string imageResource) {
    if (animationImagesMap != nullptr) {
        //(*animationImagesMap).add(frame, imageResource);
        (*animationImagesMap)[frame] = imageResource;
    }
}

void TileInfo::SetCoordinate(std::pair<int, int> coordinate) {
    this->coordinate = coordinate;
}

std::pair<int, int> TileInfo::GetCoordinate() const {
    return coordinate;
}

TileInfo TileInfo::CreateFrom(std::string tileID) {
    TileInfo tileResult = TileInfo(tileID);
    return tileResult;
}

