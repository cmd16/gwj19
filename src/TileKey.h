/* 
 * File:   TileKey.h
 * Author: raoul
 *
 * Created on 21 March 2020, 00:02
 */

#ifndef TILEKEY_H
#define TILEKEY_H

#include <utility>
#include <string>

class TileKey {
public:
    TileKey();
    TileKey(std::string id, std::pair<int, int> aPosition);
    TileKey(const TileKey& orig);
    virtual ~TileKey();
    bool operator==(const TileKey &other) const;
    std::size_t operator()(const TileKey& k) const;
    void SetPosition(std::pair<int, int> position);
    std::pair<int, int> GetPosition() const;
    void SetTileId(std::string tileId);
    std::string GetTileId() const;
    
    static TileKey GenerateTileKey(std::string id, std::pair<int, int> aPosition);
    
    std::string tileId;
    std::pair<int, int> position;
private:
    
};

class TileKeyHashFunc { 
public: 
  
    size_t operator()(const TileKey& tk) const
    { 
        return ((std::hash<std::string>()(tk.tileId)
                    ^ (std::hash<int>()(tk.position.first) << 1)) >> 1)
                    ^ (std::hash<int>()(tk.position.second) << 1);
    } 
};

#endif /* TILEKEY_H */

