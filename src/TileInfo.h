/* 
 * File:   TileInfo.h
 * Author: raoul
 *
 * Created on 17 March 2020, 00:35
 */

#ifndef TILEINFO_H
#define TILEINFO_H

#include <string>
#include <utility>
#include <memory>
#include <vector>
#include <unordered_map>

class TileInfo {
public:
    TileInfo();
    TileInfo(std::string id);
    TileInfo(const TileInfo& orig);
    virtual ~TileInfo();
    void SetPixelDimensions(std::pair<int, int> pixelDimensions);
    std::pair<int, int> GetPixelDimensions() const;
    void SetAnimFrameIndex(int animFrameIndex);
    int GetAnimFrameIndex() const;
    void SetTileID(std::string tileID);
    std::string GetTileID() const;
    void AddImageFrame(int frame, std::string imageResource);
    void SetCoordinate(std::pair<int, int> coordinate);
    std::pair<int, int> GetCoordinate() const;
    
    static TileInfo CreateFrom(std::string tileID);
private:
    std::string tileID;
    int animFrameIndex = 0; // The current frame index if many frames are used
    std::pair<int, int> pixelDimensions;
    std::pair<int, int> coordinate;
    std::unique_ptr<std::unordered_map <int, std::string>> animationImagesMap;
};

#endif /* TILEINFO_H */

