/* 
 * File:   StyleTransition.cpp
 * Author: raoul
 * 
 * Created on 17 March 2020, 23:22
 */

#include "StyleTransition.h"

StyleTransition::StyleTransition(std::string styleName) {
    this->styleName = styleName;
}

StyleTransition::StyleTransition(const StyleTransition& orig) {
}

StyleTransition::~StyleTransition() {
}

int StyleTransition::getType() {
    return styleType;
}

std::string StyleTransition::getName() {
    return styleName;
}

