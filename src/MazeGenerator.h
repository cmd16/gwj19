    /* 
 * File:   MazeGenerator.h
 * Author: raoul
 *
 * Created on 03 March 2020, 23:35
 */
#ifndef MAZEGENERATOR_H
#define MAZEGENERATOR_H

#include <random>
#include <utility>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <Godot.hpp>
#include "CavernMapGenerator.h"
#include "TileInfo.h"
#include "TileKey.h"
#include "directions.h"

namespace godot {

    class MazeGenerator {
    public:
        MazeGenerator();
        MazeGenerator(int numHorizontalTiles, int numVerticalTiles);
        MazeGenerator(const MazeGenerator& orig);
        virtual ~MazeGenerator();
        
        void generateMaze();
        void generateMaze(std::string workPath, std::string dataFile);
        void WriteMapData(std::string workPath, std::string dataFilemap, bool writeIntermediate);
        void WriteMapFile(std::string workPath, std::string dataFilemap, std::vector<uint> mapData);
        bool isWall(int column, int row);
        bool tileExists(std::string tileID);
        std::pair<int, int> getUnitTileDimension();
        std::pair<int, int> getMapTileSpan();
        std::string getTileID(int column, int row);
        void setUnitTileDimension(std::pair<int, int> tileDim);
        void setMapTileSpan(std::pair<int, int> mapTileSpan);
        bool RegionWithinBounds(uint regionNum, int x, int y, int width, int height, int blocks, std::vector<uint> regionMap);
        std::vector<uint> GetRegionTileCoordinates(uint regionNum, std::vector<uint> regionMap, int limit = 0);
        void addTileDescription(std::string tileID, std::pair<int, int> unitSize, std::string resourceImage);
        void placeTile(std::string tileID, std::pair<int, int> mapPosition);
        void placeTileFromInput(std::vector<std::string> fields);
        void modifyTilesInRow(int row, std::vector<TileInfo> tileArray);
        void ProcessMapEntry(std::string mapEntry);
        void DumpDebugMessages(std::string group);
        void ClearDebugMessages(std::string group);
        std::vector<int> CreateRandomNumberGrid(int width, int height, int minValue, int maxValue);
        std::string GetItemPlacementCoordinates(std::string item);
        
    private:
        std::mt19937_64 randomBuilder;
        std::pair<int, int> unitTileDimension; //tile size in pixels
        std::pair<int, int> mapTileSpan; //map size ito default tile unit
        std::unique_ptr<std::unordered_map <std::string, TileInfo>> mapSelectionTiles;
        std::unique_ptr<std::unordered_map <std::string, std::stringstream>> debugGroupMessages;
        std::unique_ptr<std::unordered_map <TileKey, TileInfo, TileKeyHashFunc>> mapPlacedTiles;
        std::unique_ptr<CavernMapGenerator> cavernBlobMap1 = nullptr;
        std::unique_ptr<CavernMapGenerator> cavernBlobMap2 = nullptr;
        std::vector<uint> vecGeneratedMap;
        int eraseBlockThreshold = 3;
        std::string strGateCoordinates;
        std::string strKeysCoordinates;

        void LogDebugMessage(std::string group, std::string message);
        void WriteCoordinates(std::string workPath, std::string coordFile, std::string regionTag, std::vector<uint> regionTileCoords);
        std::string WriteCoordinatesString(std::vector<uint> regionTileCoords);
        std::string WriteCoordinatesMapString(std::unordered_map<uint, std::vector<uint>> mapTileCoords);
    };
}

#endif /* MAZEGENERATOR_H */

