/* 
 * File:   GennerTileMap.h
 * Author: raoul
 *
 * Created on 22 March 2020, 10:26
 */

#ifndef GENNERTILEMAP_H
#define GENNERTILEMAP_H

#include <vector>
#include <core/Godot.hpp>
#include <Node2D.hpp>
#include <TileMap.hpp>
#include <Resource.hpp>
#include <ResourceLoader.hpp>
#include <core/Ref.hpp>
#include <core/String.hpp>
#include "MazeHolder.h"

namespace godot {

class GennerTileMap : public TileMap {
    GODOT_CLASS(GennerTileMap, TileMap)
public:
    GennerTileMap();
    GennerTileMap(const GennerTileMap& orig);
    virtual ~GennerTileMap();
    
    static void _register_methods();
    void _ready();
    void _init();
    
    void setdimension(int cols, int rows);
    void getproceduralmap();
    void setWorkingGeneratedMap(std::string workingGeneratedMap);
    std::string getWorkingGeneratedMap() const;
    std::vector<int> ReadVectorMap(std::string inputMap);
    void TransformNormalisedTiles(std::string inputMap, bool bruteForce = true);
    void GenerateTileTypesFromLayout();
    Vector2 GetIdealPlacementForPlayer(std::string inputMap);
    std::vector<Vector2> GetIdealPlacementForKeys(std::string inputMap, int keyCount);
    Variant getgateportalcoordinates();
    Variant getkeyplacementcoordinates();
private:
    int mapWidth, mapHeight;
    MazeHolder mapGeneratorAgent;
    std::string workingGeneratedMap = "gencavern1.map";
    std::unique_ptr<std::unordered_map <std::string, int>> mapTileTypes;
    
    void PopulateHardwiredTileMapping();
    bool IsSurfaceTile(std::vector<int> mapData, int x, int y);
    Vector2 GetTileVector(std::string tileType, int subId);
};

}

#endif /* GENNERTILEMAP_H */

