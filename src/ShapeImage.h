#pragma once

#include <core/Godot.hpp>
#include <Image.hpp>
#include <Node2D.hpp>
#include <Sprite.hpp>
#include <Resource.hpp>
#include <ResourceLoader.hpp>
#include <core/Ref.hpp>
#include <PackedScene.hpp>
#include <string>
#include <memory>
#include "ScriptParser.h"
#include "MazeGenerator.h"

namespace godot {

    class ShapeImage : public Image {
        GODOT_CLASS(ShapeImage, Image)
    private:
        int imgWidth, imgHeight;
        std::string specificationRes;
        std::string selectItem;
        ScriptParser scriptParser;
        godot::Ref<PackedScene> shapeImageResource;
        //Sprite drawSprite;
    public:
        static void _register_methods();
        void _ready();
        void _init();

        ShapeImage();
        ShapeImage(std::string imageResource);
        ~ShapeImage();
    };
}
