/* 
 * File:   GennerTileMap.cpp
 * Author: raoul
 * 
 * Created on 22 March 2020, 10:26
 */

#include <algorithm> 
#include <cctype>
#include <TileSet.hpp>
#include <core/CoreTypes.hpp>
#include <core/Ref.hpp>
#include "GennerTileMap.h"
#include "extrahelper.h"

using namespace godot;

// trim from start (in place)

static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)

static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)

static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

void GennerTileMap::_register_methods() {
    register_method((char*) "_ready", &GennerTileMap::_ready);
    register_method((char*) "setdimension", &GennerTileMap::setdimension);
    register_method((char*) "getproceduralmap", &GennerTileMap::getproceduralmap);
    register_method((char*) "extracttileids", &GennerTileMap::GenerateTileTypesFromLayout);
    register_method((char*) "getgateportalcoordinates", &GennerTileMap::getgateportalcoordinates);
    register_method((char*) "getkeyplacementcoordinates", &GennerTileMap::getkeyplacementcoordinates);
}

void GennerTileMap::PopulateHardwiredTileMapping() {
    if (mapTileTypes != nullptr) {
        (*mapTileTypes)["ground"] = 2;
        (*mapTileTypes)["grass"] = 3;
    }
}

void GennerTileMap::GenerateTileTypesFromLayout() {
    char debugText[8192];
    godot::Ref<TileSet> tgSet = get_tileset();
    godot::Array idArray = tgSet->get_tiles_ids(); //Array
    memset(debugText, 0, 8192);
    snprintf(debugText, 8191, "GennerTileMap::GenerateTileTypesFromLayout(), tileset size=% d", idArray.size());
    Godot::print(debugText);
    if (idArray.size() >= 2) {
        Variant v1 = idArray[0];
        Variant v2 = idArray[1];
        String tile1 = tgSet->tile_get_name(v1);
        String tile2 = tgSet->tile_get_name(v2);
        /*
        std::vector<int> tileIDtypes;
        tileIDtypes.push_back(get_cell(0, 0));
        tileIDtypes.push_back(get_cell(1, 0));
         * */
        //std::string debugVecData = ExtraHelper::printDebugVector("", tileIDtypes);
        memset(debugText, 0, 8192);
        snprintf(debugText, 8191, "GennerTileMap::GenerateTileTypesFromLayout(), tile1=%s, tile2=%s", tile1.alloc_c_string(), tile2.alloc_c_string());
        Godot::print(debugText);
        if (mapTileTypes != nullptr) {
            (*mapTileTypes)["ground"] = v1; //godot::Variant.to_i64(v1);
            (*mapTileTypes)["grass"] = v2; //tileIDtypes[1];
        }
    }
}

bool GennerTileMap::IsSurfaceTile(std::vector<int> mapData, int x, int y) {
    bool res = false;
    if (mapData.size() >= (mapWidth * mapHeight)) {
        if (y > 0) {
            int value = mapData[x + ((y - 1) * mapWidth)];
            if (x == 0) {
                if (value <= 0)
                    res = true;
            } else {
                int value3 = 0;
                int value2 = mapData[ (x - 1) + ((y - 1) * mapWidth)];
                if (x < mapWidth - 1)
                    value3 = mapData[ (x + 1) + ((y - 1) * mapWidth)];
                if ( (value <= 0) && (value2 <= 0) )
                    res = true;
            }
        }
    }
    return res;
}

Vector2 GennerTileMap::GetTileVector(std::string tileType, int subId) {
    Vector2 res;
    if (tileType == "grass") {
        switch (subId) {
            case 0:
                res.x = 0; res.y = 0;
                break;
            case 1:
                res.x = 3; res.y = 0; //SW
                break;
            case 2:
                res.x = 4; res.y = 0; //SE
                break;
            case 3:
                res.x = 5; res.y = 0; //NE
                break;
            case 4:
                res.x = 6; res.y = 0; //NW
                break;
        }
    }
    if (tileType == "soil") {
        switch (subId) {
            case 0:
                res.x = 0; res.y = 1; //CORE
                break;
            case 1:
                res.x = 3; res.y = 1; //SW
                break;
            case 2:
                res.x = 4; res.y = 1; //SE
                break;
            case 3:
                res.x = 5; res.y = 1; //NE
                break;
            case 4:
                res.x = 6; res.y = 1; //NW
                break;
        }
    }
    return res;
}

Variant GennerTileMap::getgateportalcoordinates() {
    Variant res = mapGeneratorAgent.getgateportalcoordinates();
    
    return res;
}

Variant GennerTileMap::getkeyplacementcoordinates() {
    Variant res = mapGeneratorAgent.getkeyplacementcoordinates();
    
    return res;
}

void GennerTileMap::_ready() {
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "GennerTileMap::_ready() called");
    Godot::print(debugText);
    mapTileTypes = std::make_unique<std::unordered_map <std::string, int> >();
    //PopulateHardwiredTileMapping();
    GenerateTileTypesFromLayout();
    mapGeneratorAgent._ready();
}

void GennerTileMap::_init() {
}

void GennerTileMap::setdimension(int cols, int rows) {
    mapWidth = cols;
    mapHeight = rows;
    mapGeneratorAgent.setdimension(mapWidth, mapHeight);
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "GennerTileMap::setdimension, cols=%d rows=%d \n", cols, rows);
    Godot::print(debugText);
}

void GennerTileMap::getproceduralmap() {
    char debugText[1024];
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "GennerTileMap::getproceduralmap() called, workingGeneratedMap=%s\n", workingGeneratedMap.c_str());
    Godot::print(debugText);
    mapGeneratorAgent.generateautolevel();
    mapGeneratorAgent.autoleveldatamap(workingGeneratedMap.c_str());
    std::string mapGenFilePath = ExtraHelper::combineFilePath(ExtraHelper::getWorkingPath(), workingGeneratedMap);
    memset(debugText, 0, 1024);
    snprintf(debugText, 1023, "GennerTileMap::getproceduralmap() called, mapGenFilePath=%s\n", mapGenFilePath.c_str());
    Godot::print(debugText);
    if (ExtraHelper::FileExists(mapGenFilePath)) {
        TransformNormalisedTiles(workingGeneratedMap);
        memset(debugText, 0, 1024);
        snprintf(debugText, 1023, "GennerTileMap::getproceduralmap() , Transform completed");
        Godot::print(debugText);
    }
}

std::vector<int> GennerTileMap::ReadVectorMap(std::string inputMap) {
    std::vector<int> res;
    if (inputMap.length() >= 3) {
        std::vector<std::string> mapTextStream = ExtraHelper::readFileAsVector(inputMap);
        int idx = 0;
        for (std::string inLine : mapTextStream) {
            if (idx == 0) {
                //read width and height
            } else {
                std::vector<std::string> valueLine = ExtraHelper::SplitTextOn(inLine, "=");
                if (valueLine.size() >= 2) {
                    std::string input = valueLine[1];
                    trim(input);
                    for (int ix = 0; ix < input.length(); ix++) {
                        res.push_back(input[ix] - '0');
                    }
                }
            }
            idx++;
        }
    }
    return res;
}

void GennerTileMap::TransformNormalisedTiles(std::string inputMap, bool bruteForce) {
    if (inputMap.length() >= 3) {
        std::vector<int> generatedData = ReadVectorMap(inputMap);
        char debugText[8192];
        /*
        std::string debugVecData = ExtraHelper::printDebugVector("", generatedData);        
        memset(debugText, 0, 8192);
        snprintf(debugText, 8191, "GennerTileMap::TransformNormalisedTiles(), size=%ld, data=%s", generatedData.size(), debugVecData.c_str());
        Godot::print(debugText);
         * */
        int groundTileID = (*mapTileTypes)["ground"];
        int grassTileID = (*mapTileTypes)["grass"];
        if (generatedData.size() >= (mapHeight * mapWidth)) {
            for (int ix = 0; ix < mapWidth; ix++) {
                for (int iy = 0; iy < mapHeight; iy++) {
                    if (bruteForce) {
                        if (generatedData[ix + (iy * mapWidth)] >= 1)
                            if (IsSurfaceTile(generatedData, ix, iy))
                                set_cell(ix, iy, groundTileID, false, false, false, GetTileVector("grass", 0));
                            else
                                set_cell(ix, iy, groundTileID, false, false, false, GetTileVector("soil", 0));
                    } else {
                        set_cell(ix, iy, -1);
                    }
                }
            }
        }
    }
}

Vector2 GennerTileMap::GetIdealPlacementForPlayer(std::string inputMap) {
    //Find a platform around the centre that is large enough
    //Should have flat area spanning 4 or 5 horizontal tiles
    std::vector<int> generatedData = ReadVectorMap(inputMap);
}

std::vector<Vector2> GennerTileMap::GetIdealPlacementForKeys(std::string inputMap, int keyCount) {
    //Find relatively flat surface areas in regions with moderate area size
    std::vector<int> generatedData = ReadVectorMap(inputMap);
    //FeaturesQueryMap queryMap(generatedData);
    //std::vector<MapRegion> = queryMap.FindRegionsFromCentre(radius, continuousFlat, minVertices);
}

void GennerTileMap::setWorkingGeneratedMap(std::string workingGeneratedMap) {
    this->workingGeneratedMap = workingGeneratedMap;
}

std::string GennerTileMap::getWorkingGeneratedMap() const {
    return workingGeneratedMap;
}

GennerTileMap::GennerTileMap() {
}

GennerTileMap::GennerTileMap(const GennerTileMap& orig) {
}

GennerTileMap::~GennerTileMap() {
}

