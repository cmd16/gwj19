#ifndef EXTRA_HELPER_H
#define EXTRA_HELPER_H

#include <Godot.hpp>
#include <vector>
#include <sstream>
#include <cstring>
#include <sys/stat.h>
#include <unistd.h>

namespace godot {

    class ExtraHelper {
    public:
        static std::string printVector2(Vector2 vec, std::string tag = "");
        
        static std::vector<std::string> readFileAsVector(std::string dataFile);
        
        static std::string getWorkingPath();
        
        static std::string combineFilePath(std::string aPath, std::string aFile);
        
        static std::string printShortTimestamp();
        
        static std::vector<std::string> SplitTextOn(std::string mainStr, std::string split, bool includeSplit = false);
        
        static inline bool FileExists(std::string aFile) {
            struct stat buffer;   
            return (stat (aFile.c_str(), &buffer) == 0);
        }
        
        template <typename T>
        static std::string printDebugVector(std::string tag, std::vector<T> vectorList) {
            std::stringstream stringreader;
            for (T item : vectorList) {
                stringreader << item << std::endl;
                //strncat(debugText, stringreader.str().c_str(), stringreader.str().length()  );
            }
            return stringreader.str();
        }
        
	template <typename T>
	static void printGodotDebugVector(std::string tag, std::vector<T> vectorList) {
            char debugText[4096 * 4];
            std::stringstream stringreader;
            memset(debugText, 0, 4096 * 4);
            //snprintf(debugText, 4095, "printGodotDebugVector TEST");
            //Godot::print(debugText);
            for (T item : vectorList) {
                stringreader << item << std::endl;
                strncat(debugText, stringreader.str().c_str(), stringreader.str().length()  );
            }
            Godot::print(debugText);
        }
    };
}

#endif
