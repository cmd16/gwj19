#pragma once

#include <core/Godot.hpp>
#include <Node2D.hpp>
#include <Resource.hpp>
#include <ResourceLoader.hpp>
#include <core/Ref.hpp>
#include <core/String.hpp>
#include <PackedScene.hpp>
#include <string>
#include <memory>
#include <utility>
#include "MazeGenerator.h"

namespace godot {

    class MazeHolder : public Node2D {
        GODOT_CLASS(MazeHolder, Node2D)
    private:
        std::unique_ptr<MazeGenerator> mazeObject;
        //Node2D*** mazeNodes;
        godot::Ref<PackedScene> wallScene;
        std::string workPath;
        int mazeWidth, mazeHeight;
        std::unique_ptr<std::unordered_map <std::string, std::pair<float, float>>> mapTileUsageRatios; //1st is proportion, and 2d is tolerance on that proportion

        std::string createNameValue(int x, int y, std::string tileCategory = "");
    public:
        static void _register_methods();
        void _ready();
        void _init();

        MazeHolder(std::string resourceScene);
        MazeHolder();
        ~MazeHolder();
        void setdimension(int cols, int rows);
        void generatelevel(String mapfile);
        void generateautolevel();
        void autoleveldatamap(String mapfile);
        void dumpdebug(String section);
        void cleardebug(String section);
        Variant getgateportalcoordinates();
        Variant getkeyplacementcoordinates();
        void AddTileUsage(String tileSection, float proportion, float tolerance); //tileSection is like Air, Water, Wall, Crust ....

        //void AddWallsToMaze();
    };
}
