/* 
 * File:   ConnectedComponentsFinder.h
 * Author: raoul
 *
 * Created on 22 March 2020, 14:56
 */

#ifndef CONNECTEDCOMPONENTSFINDER_H
#define CONNECTEDCOMPONENTSFINDER_H

#include <vector>
#include <unordered_map>
#include <map>
#include <memory>
#include <string>
#include <algorithm>
#include "directions.h"

const static uint OUT_COLOR = UINT_MAX - 1;

class ConnectedComponentsFinder {
public:
    struct cnode {
        cnode *parent;
        uint label;

        cnode(uint label) {
            this -> label = label;
            this -> parent = this;
        }
    };
    ConnectedComponentsFinder(std::vector<int> input);
    ConnectedComponentsFinder(std::vector<int> input, int width, int height);
    ConnectedComponentsFinder(std::vector<uint> input, int width, int height);
    ConnectedComponentsFinder(const ConnectedComponentsFinder& orig);
    virtual ~ConnectedComponentsFinder();

    void SetDimension(int width, int height);
    void FindConnectedComponentsInput();
    std::vector<uint> GetOutVertices();

    template<uint connectivity> void FirstPass(const std::vector<uint>& img, std::vector<uint>& res, uint xLen, uint yLen, std::map<uint, cnode *>& labToNode, uint &currLabel) {
        uint left, up, leftup, rightup, curr, currPos, leftC, upC, leftupC, rightupC;
        for (int i = 0; i < (int) yLen; ++i) {
            for (int j = 0; j < (int) xLen; ++j) {
                if (i == 0 && j == 0) continue;
                currPos = i * xLen + j;
                curr = img[currPos];
                left = GetW(currPos, xLen);
                up = GetN(currPos, xLen);
                leftC = left == ERROR_CODE ? OUT_COLOR : img[left];
                upC = up == ERROR_CODE ? OUT_COLOR : img[up];
                std::vector<uint> match;
                if (leftC == curr) match.push_back(res[left]);
                if (upC == curr) match.push_back(res[up]);
                if (connectivity == 8) {
                    leftup = GetNW(currPos, xLen);
                    rightup = GetNE(currPos, xLen);
                    leftupC = leftup == ERROR_CODE ? OUT_COLOR : img[leftup];
                    rightupC = rightup == ERROR_CODE ? OUT_COLOR : img[rightup];
                    if (leftupC == curr) match.push_back(res[leftup]);
                    if (rightupC == curr) match.push_back(res[rightup]);
                }
                if (match.size() == 0) {
                    res[currPos] = currLabel;
                    AddLabel(currLabel, labToNode);
                } else {
                    std::sort(match.begin(), match.end());
                    uint ref = match[0];
                    res[currPos] = ref;
                    for (int i = 1; i < (int) match.size(); ++i) {
                        if (match[i] != ref) {
                            Union(ref, match[i], labToNode);
                        }
                    }
                }
            }
        }
    }
private:
    std::vector<uint> inputVertices;
    std::unique_ptr<std::vector<uint>> outVertices = nullptr; // res(vec.size());
    int conHeight;
    int conWidth;

    void ConvertIntVector(std::vector<int> input);
    void FindConnectedComponents(const std::vector<uint>& img, uint xLen, uint yLen, uint connectivity);
    void Union(uint lab1, uint lab2, std::map<uint, cnode *>& labToNode);
    cnode* FindRep(cnode *n);
    void Union(cnode *x, cnode *y);
    void AddLabel(uint& label, std::map<uint, cnode *>& labToNode);
    void SecondPass(std::map<uint, cnode *>& labToNode, std::vector<uint>& res, uint xLen, uint yLen, uint maxLabel);
};

#endif /* CONNECTEDCOMPONENTSFINDER_H */

